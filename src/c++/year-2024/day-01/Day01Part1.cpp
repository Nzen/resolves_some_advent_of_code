
// retyping https://rosettacode.org/wiki/Category:C%2B%2B

#include <algorithm>
#include <fstream>
#include <iostream>
#include <sstream>
using namespace std;
#include <string>
#include <vector>

int main(
	int argc, char** argv
) {
	std::vector<int> collectLeft;
	std::vector<int> collectRight;
	int lineCount = 0;
	std::string line;
	std::string token;
	std::ifstream infile( argv[ 1 ] ); // supposedly input file steam

	if ( infile ) {
		while ( getline( infile, line ) ) {
			// std::cout << lineCount << ": " << line << '\n';
			std::istringstream ss( line );
			bool hasSavedLeft = false;

			while( std::getline( ss, token, ' ' ) ) {
				if ( token.length() < 1 )
					continue;
				else if ( ! hasSavedLeft ) {
					int left;
					istringstream convert( token ); // because I'm using mingw, reputedly
						// https://cplusplus.com/forum/beginner/120836/#msg657704

					if ( ! ( convert >> left ) )
						left = 0;

					// int left = std::stoi( token );
						// the typical c++ v11 invocation
					collectLeft.push_back( left );
					// std::cout << left << "\n";
					hasSavedLeft = true;
				}
				else {
					int right;
					istringstream convert( token ); // because I'm using mingw, reputedly

					if ( ! ( convert >> right ) )
						right = 0;

					// int right = std::stoi( token );
					collectRight.push_back( right );
					// std::cout << right << "\n";
				}
				// std::cout << '|' << token << "|\n";
			}

			lineCount += 1;
		}
	}

	infile.close();

	std::sort( collectLeft.begin(), collectLeft.end() );
	std::sort( collectRight.begin(), collectRight.end() );
	int totalDistance = 0;

	for ( int sortedIndex = 0; sortedIndex < collectLeft.size(); sortedIndex += 1 ) {
		int leftValue = collectLeft[ sortedIndex ];
		int rightValue = collectRight[ sortedIndex ];
		int distance;

		if ( leftValue == rightValue )
			distance = 0;
		else if ( leftValue > rightValue )
			distance = leftValue - rightValue;
		else
			distance = rightValue - leftValue;

		totalDistance += distance;
	}

	std::cout << "\ttotal distance " << totalDistance << "\n";
	return 0;
}
