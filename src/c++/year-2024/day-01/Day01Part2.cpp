
// retyping https://rosettacode.org/wiki/Category:C%2B%2B

#include <algorithm>
#include <fstream>
#include <iostream>
#include <map>
#include <sstream>
using namespace std;
#include <string>
#include <vector>

int main(
	int argc, char** argv
) {
	std::vector<int> collectLeft;
	std::map<int,int> rightFrequencies;
	int lineCount = 0;
	std::string line;
	std::string token;
	std::ifstream infile( argv[ 1 ] ); // supposedly input file steam

	if ( infile ) {
		while ( getline( infile, line ) ) {
			// std::cout << lineCount << ": " << line << '\n';
			std::istringstream ss( line );
			bool hasSavedLeft = false;

			while( std::getline( ss, token, ' ' ) ) {
				if ( token.length() < 1 )
					continue;
				else if ( ! hasSavedLeft ) {
					int left;
					istringstream convert( token ); // because I'm using mingw, reputedly
						// https://cplusplus.com/forum/beginner/120836/#msg657704

					if ( ! ( convert >> left ) )
						left = 0;

					// int left = std::stoi( token );
						// the typical c++ v11 invocation
					collectLeft.push_back( left );
					// std::cout << left << "\n";
					hasSavedLeft = true;
				}
				else {
					int right;
					istringstream convert( token ); // because I'm using mingw, reputedly

					if ( ! ( convert >> right ) )
						right = 0;

					// int right = std::stoi( token );
					// std::cout << right << "\n";

					if ( rightFrequencies.count( right ) > 0 )
						rightFrequencies[ right ] += 1;
					else
						rightFrequencies[ right ] = 1;

					// std::cout << "r-" << right << " rf-" << rightFrequencies[ right ] << "\n";
				}
				// std::cout << '|' << token << "|\n";
			}

			lineCount += 1;
		}
	}

	infile.close();
	int totalSimilarity = 0;

	for ( int sortedIndex = 0; sortedIndex < collectLeft.size(); sortedIndex += 1 ) {
		int leftValue = collectLeft[ sortedIndex ];
		int rightOccurrances = rightFrequencies[ leftValue ];
		int similarity = leftValue * rightOccurrances;
		// std::cout << "lv-" << leftValue << " rv-" << rightOccurrances << " s-" << similarity << "\n";

		totalSimilarity += similarity;
	}

	std::cout << "\ttotal Similarity " << totalSimilarity << "\n";
	return 0;
}
