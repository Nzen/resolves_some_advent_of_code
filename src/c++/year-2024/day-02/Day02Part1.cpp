
// retyping https://rosettacode.org/wiki/Category:C%2B%2B

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

int main(
	int argc, char** argv
) {
	int lineCount = 0;
	std::string line;
	std::ifstream infile( argv[ 1 ] );
	std::vector<std::string> lines;

	if ( infile ) {
		while ( getline( infile, line ) ) {
			lines.push_back( line );
			// std::cout << lineCount << ": " << line << "\n";

			lineCount += 1;
		}
	}

	infile.close();

	int totalGoodReports = 0;
	std::string token;

	for ( int index = 0; index < lines.size(); index += 1 ) {
		line = lines[ index ];
		std::istringstream ss( line );
		bool isIncreasing = 0;
		bool consistent = true;
		int previousValue;
		int currentValue;
		int valuesSeen = 0;

		while( std::getline( ss, token, ' ' ) ) {
			if ( token.length() < 1 )
				continue;
			else {
				currentValue = std::stoi( token );

				if ( valuesSeen > 0 ) {
					if ( valuesSeen == 1 ) {
						if ( currentValue == previousValue
								|| ( currentValue - previousValue ) > 3
								|| ( currentValue - previousValue ) < -3 ) {
							consistent = false;
							break;
						}

						isIncreasing = currentValue > previousValue;
					}
					else if ( ( isIncreasing
								&& ( currentValue <= previousValue
									|| ( currentValue - previousValue ) > 3 ) )
							|| ( ! isIncreasing
								&& ( currentValue >= previousValue
									|| ( currentValue - previousValue ) < -3 ) ) ) {
						consistent = false;
						break;
					}
				}

				previousValue = currentValue;
				valuesSeen += 1;
			}
		}

		if ( consistent ) {
			totalGoodReports += 1;
		}
		
	}

	std::cout << "tgr " << totalGoodReports << "\n";

	return 0;
}
