
// retyping https://rosettacode.org/wiki/Category:C%2B%2B

#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

int main(
	int argc, char** argv
) {
	int lineCount = 0;
	std::string line;
	std::ifstream infile( argv[ 1 ] );
	std::vector<std::string> lines;

	if ( infile ) {
		while ( getline( infile, line ) ) {
			if ( line.length() < 1 )
				continue;

			lines.push_back( line );
			// std::cout << lineCount << ": " << line << "\n";
			lineCount += 1;
		}
	}

	infile.close();

	int totalGoodReports = 0;
	std::string token;
		std::vector<int> reportsOfLines;

	for ( int lineIndex = 0; lineIndex < lines.size(); lineIndex += 1 ) {
		line = lines[ lineIndex ];
		std::istringstream ss( line );
		reportsOfLines.clear();

		// ¶ convert all numbers, so we know the length up front
		while( std::getline( ss, token, ' ' ) ) {
			if ( token.length() < 1 )
				continue;
			else {
				reportsOfLines.push_back( std::stoi( token ) );
			}
		}

		if ( reportsOfLines.size() == 1 || reportsOfLines.size() == 2 ) {
			totalGoodReports += 1;
			continue;
		}

		// ¶ n^2 search skipping one per loop
		bool foundOneWhileSkipping = false;

		for ( int skipIndex = 0; skipIndex < reportsOfLines.size(); skipIndex += 1 ) {
			bool isIncreasing = false;
			bool consistent = true;
			int previousValue;
			int currentValue;
			int valuesSeen = 0;

			for ( int checkingIndex = 0; checkingIndex < reportsOfLines.size(); checkingIndex += 1 ) {
				if ( checkingIndex == skipIndex )
					continue;

				currentValue = reportsOfLines[ checkingIndex ];

				if ( valuesSeen > 0 ) {
					if ( valuesSeen == 1 ) {
						if ( currentValue == previousValue
								|| ( currentValue - previousValue ) > 3
								|| ( currentValue - previousValue ) < -3 ) {
							consistent = false;
							break;
						}

						isIncreasing = currentValue > previousValue;
					}
					else if ( ( isIncreasing
								&& ( currentValue <= previousValue
									|| ( currentValue - previousValue ) > 3 ) )
							|| ( ! isIncreasing
								&& ( currentValue >= previousValue
									|| ( currentValue - previousValue ) < -3 ) ) ) {
						consistent = false;
						break;
					}
				}

				previousValue = currentValue;
				valuesSeen += 1;
			}

			if ( consistent ) {
				foundOneWhileSkipping = true;
				break;
			}
		}

		if ( foundOneWhileSkipping ) {
			totalGoodReports += 1;
			continue;
		}
	}

	std::cout << "tgr " << totalGoodReports << "\n";

	return 0;
}
