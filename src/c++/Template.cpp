
// retyping https://rosettacode.org/wiki/Category:C%2B%2B

#include <fstream>
#include <string>
#include <iostream>

int main(
	int argc, char** argv
) {
	int lineCount = 0;
	std::string line;
	std::ifstream infile( argv[ 1 ] ); // supposedly input file steam

	if ( infile ) {
		while ( getline( infile, line ) ) {
			std::cout << lineCount << ": " << line << '\n';

			lineCount += 1;
		}
	}

	infile.close();
	return 0;
}
