
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeSet;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Exercise18031
{
	private class Rectangle
	{
		String rectId = "";
		int posX = 0;
		int posY = 0;
		int lenX = 0;
		int lenY = 0;
		boolean beenChecked = false;
		public String toString()
		{
			return String.format( "id-%s px-%d py-%d lx-%d ly-%d",
					rectId, posX, posY, lenX, lenY );
		}
	}
	private static final String cl = "e18031.";

	public static void main( String args[] )
	{
		final String here = cl +"m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		Exercise18031 solvesTheProblem = new Exercise18031();
		solvesTheProblem.with( fileLines );
	}


	void with( List<String> fileLines )
	{
		final String here = cl +"w ";
		// 4TESTS
		fileLines.clear();
		fileLines.add( "#11 @ 1,1: 3x3" );
		fileLines.add( "#22 @ 2,1: 3x3" );
		fileLines.add( "#33 @ 1,2: 3x3" );
		// fileLines.add( "#44 @ 5,4: 1x1" );
		// fileLines.add( "#55 @ 1,6: 7x3" );
		// 4TESTS
		List<Rectangle> userRectangles = parse( fileLines );
		Map<String, Rectangle> allRect = new TreeMap<>();
		NavigableMap<Integer, List<String>> xxPositions = new TreeMap<>();
		NavigableMap<Integer, List<String>> yyPositions = new TreeMap<>();
		for ( Rectangle currRect : userRectangles )
		{
			allRect.put( currRect.rectId, currRect );
			List<String> shapesAtXx = xxPositions.get( currRect.posX );
			if ( shapesAtXx == null )
			{
				shapesAtXx = new LinkedList<>();
			}
			shapesAtXx.add( currRect.rectId );
			xxPositions.put( currRect.posX, shapesAtXx );
			List<String> shapesAtYy = yyPositions.get( currRect.posY );
			if ( shapesAtYy == null )
			{
				shapesAtYy = new LinkedList<>();
			}
			shapesAtYy.add( currRect.rectId );
			yyPositions.put( currRect.posY, shapesAtYy );
		}
		String delimiter = "@@", separator = "^^";
		Set<String> bothOcclude = occlusionPairs(
				userRectangles, xxPositions, yyPositions, delimiter );
		// NOTE create new rectangles at the intersection
		Map<String, Rectangle> allJoins = new TreeMap<>();
		xxPositions.clear();
		yyPositions.clear();
		for ( String comboId : bothOcclude )
		{
			String[] pair = comboId.split( delimiter );
			Rectangle first = allRect.get( pair[0 ] );
			Rectangle last = allRect.get( pair[ 1 ] );
			Rectangle intersect = intersectionOf( first, last, comboId );
			System.out.println( here +"occluded at "+ intersect ); // 4TESTS
			allJoins.put( comboId, intersect );
			List<String> shapesAtXx = xxPositions.get( intersect.posX );
			if ( shapesAtXx == null )
			{
				shapesAtXx = new LinkedList<>();
			}
			shapesAtXx.add( intersect.rectId );
			xxPositions.put( intersect.posX, shapesAtXx );
			List<String> shapesAtYy = yyPositions.get( intersect.posY );
			if ( shapesAtYy == null )
			{
				shapesAtYy = new LinkedList<>();
			}
			shapesAtYy.add( intersect.rectId );
			yyPositions.put( intersect.posY, shapesAtYy );
		}
		System.out.println( here +"combined area is "+ overlappingAreaOf( allJoins, xxPositions, yyPositions, separator ) );
	}


	/** format: #[[ number ]] @ [[ x ]],[[ y ]]: [[ width ]]x[[ height ]]
		ex: #121 @ 55,2424: 224x2424 */
	List<Rectangle> parse( List<String> rectangleDesc )
	{
		final String here = cl +"p ";
		List<Rectangle> rectangles = new LinkedList<>();
		int idInd = 1, xposInd = idInd +1, yposInd = xposInd +1,
				xlenInd = yposInd +1, ylenInd = xlenInd +1; // one indexed because 0 is the whole thing
		int currInd = 0;
		String rectangleFormat = "#(\\d+) @ (\\d+),(\\d+): (\\d+)x(\\d+)";
		String expression = "";
		try
		{
			Pattern fsmSpec = Pattern.compile( rectangleFormat );
			Matcher fsmRuntime;
			for ( String currInput : rectangleDesc )
			{
				expression = currInput; // 4TESTS ensuring that I can get the problematic line, otherwise unnecessary
				fsmRuntime = fsmSpec.matcher( currInput );
				Rectangle currRect = new Rectangle();
				if ( fsmRuntime.find( currInd ) )
				{
					currRect.rectId = fsmRuntime.group( idInd );
					currRect.posX = Integer.valueOf( fsmRuntime.group( xposInd ) );
					currRect.posY = Integer.valueOf( fsmRuntime.group( yposInd ) );
					currRect.lenX = Integer.valueOf( fsmRuntime.group( xlenInd ) );
					currRect.lenY = Integer.valueOf( fsmRuntime.group( ylenInd ) );
					rectangles.add( currRect );
				}
			}
		}
		catch ( IllegalArgumentException | IllegalStateException iae )
		{
			System.err.println( here +"couldn't interpret "+ expression
					+" as a rectangle "+ iae );
		}
		return rectangles;
	}


	Set<String> occlusionPairs( List<Rectangle> userRectangles,
			NavigableMap<Integer, List<String>> xxPositions,
			NavigableMap<Integer, List<String>> yyPositions,
			String delimiter )
	{
		Set<String> bothOcclude = new TreeSet<>();
		final boolean inclusive = true;
		for ( Rectangle currRect : userRectangles )
		{
			NavigableMap<Integer, List<String>> xxOcclusions = xxPositions.subMap(
					 currRect.posX, inclusive, currRect.posX + currRect.lenX, ! inclusive );
			NavigableMap<Integer, List<String>> yyOcclusions = yyPositions.subMap(
					currRect.posY, inclusive, currRect.posY + currRect.lenY, ! inclusive );
			Set<String> inXxShadow = new TreeSet<>();
			Set<String> inYyShadow = new TreeSet<>();
			for ( Integer currXx : xxOcclusions.keySet() )
			{
				List<String> idsAtXx = xxOcclusions.get( currXx );
				for ( String idAt : idsAtXx )
				{
					if ( idAt.equals( currRect.rectId ) )
					{
						continue;
					}
					else
					{
						inXxShadow.add( idAt );
					}
				}
			}
			for ( Integer currYy : yyOcclusions.keySet() )
			{
				List<String> idsAtYy = yyOcclusions.get( currYy );
				for ( String idAt : idsAtYy )
				{
					if ( idAt.equals( currRect.rectId ) )
					{
						continue;
					}
					else
					{
						inYyShadow.add( idAt );
					}
				}
			}
			// NOTE find the intersection of the two sets, these actually overlap the current rectangle
			for ( String xxId : inXxShadow )
			{
				for ( String yyId : inYyShadow )
				{
					if ( xxId.equals( yyId )
						&&  ! bothOcclude.contains( xxId + delimiter + currRect.rectId ) ) // avoiding inserting when we find the reverse of an existing pair
					{
						bothOcclude.add( currRect.rectId + delimiter + xxId );
					}
				}
			}
		}
		return bothOcclude;
	}


	int overlappingAreaOf( Map<String, Rectangle> allJoins,
			NavigableMap<Integer, List<String>> xxPositions,
			NavigableMap<Integer, List<String>> yyPositions,
			String delimiter )
	{
		int combinedArea = 0;
		final boolean inclusive = true;
		boolean multiOverlap;
		for ( String pairId : allJoins.keySet() )
		{
			multiOverlap = false;
			Rectangle currRect = allJoins.get( pairId );
			NavigableMap<Integer, List<String>> xxOcclusions = xxPositions.subMap(
					 currRect.posX, inclusive, currRect.posX + currRect.lenX, ! inclusive );
			NavigableMap<Integer, List<String>> yyOcclusions = yyPositions.subMap(
					currRect.posY, inclusive, currRect.posY + currRect.lenY, ! inclusive );
			Set<String> inXxShadow = new TreeSet<>();
			Set<String> inYyShadow = new TreeSet<>();
			for ( Integer currXx : xxOcclusions.keySet() )
			{
				List<String> idsAtXx = xxOcclusions.get( currXx );
				for ( String idAt : idsAtXx )
				{
					if ( idAt.equals( currRect.rectId ) )
					{
						continue;
					}
					else
					{
						inXxShadow.add( idAt );
						multiOverlap |= true;
					}
				}
			}
			for ( Integer currYy : yyOcclusions.keySet() )
			{
				List<String> idsAtYy = yyOcclusions.get( currYy );
				for ( String idAt : idsAtYy )
				{
					if ( idAt.equals( currRect.rectId ) )
					{
						continue;
					}
					else
					{
						inYyShadow.add( idAt );
					}
				}
			}
			// NOTE find the intersection of the two sets, these actually overlap the current rectangle
			for ( String xxId : inXxShadow )
			{
				for ( String yyId : inYyShadow )
				{
					if ( xxId.equals( yyId ) )
					{
						Rectangle first = allJoins.get( xxId );
						Rectangle intersect = intersectionOf( first, currRect, delimiter );
						if ( ! first.beenChecked || ! currRect.beenChecked )
						{
							// if they've both been checked, then this is a really multi overlap (3 pairs) avoiding overdraft
							combinedArea -= intersect.lenX * intersect.lenY;
						}
						if ( ! first.beenChecked )
						{
							combinedArea += first.lenX * first.lenY;
							first.beenChecked = true;
						}
						if ( ! currRect.beenChecked )
						{
							combinedArea += currRect.lenX * currRect.lenY;
							currRect.beenChecked = true;
						}
						multiOverlap |= true;
					}
				}
			}
			if ( ! multiOverlap )
			{
				combinedArea += currRect.lenX * currRect.lenY;
				currRect.beenChecked = true;
			}
			// else handled above
		}
		return combinedArea;
	}


	Rectangle intersectionOf( Rectangle some, Rectangle other, String idToUse )
	{
		Rectangle between = new Rectangle();
		between.rectId = idToUse;
		between.posX = Math.max( some.posX, other.posX );
		between.posY = Math.max( some.posY, other.posY );
		int endSome = some.posX + some.lenX;
		int endOther = other.posX + other.lenX;
		between.lenX = Math.min( endSome, endOther ) - between.posX;
		endSome = some.posY + some.lenY;
		endOther = other.posY + other.lenY;
		between.lenY = Math.min( endSome, endOther ) - between.posY;
		return between;
	}

}



























