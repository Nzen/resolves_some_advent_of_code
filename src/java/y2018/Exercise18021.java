
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Exercise18021
{

	public static void main( String args[] )
	{
		final String here = "e18021.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		make a map
		make a trackingVal for pairs and triple chars
		iterate over the list of words
		  clear the map
		  iterate over the characters of the current word
			at each character, add 1 to map at that position
		  iterate over the map characters
		  |= two bools: exactly 2 and exactly 3 occurances
		  if bool of occurance is true, add 1 to that trackingVal
		multiply the values of the pairs against the triples, that's the answer
		*/
		Map<Character, Integer> charDistribution = new TreeMap<>();
		int pairCount = 0, tripleCount = 0;
		for ( String word : fileLines )
		{
			if ( word.isEmpty() )
			{
				continue;
			}
			charDistribution.clear();
			for ( int ind = word.length() -1; ind >= 0; ind-- )
			{
				Character nibble = word.charAt( ind );
				Integer nibbleSeen = charDistribution.get( nibble );
				if ( nibbleSeen == null )
				{
					nibbleSeen = Integer.valueOf( 0 );
					charDistribution.put( nibble, nibbleSeen );
				}
				nibbleSeen = Integer.valueOf( nibbleSeen.intValue() +1 );
				charDistribution.put( nibble, nibbleSeen );
			}
			boolean foundPair = false, foundTriple = false;
			for ( Character nibble : charDistribution.keySet() )
			{
				Integer nibbleTimes = charDistribution.get( nibble );
				if ( nibbleTimes == 2 )
				{
					foundPair |= true;
				}
				if ( nibbleTimes == 3 )
				{
					foundTriple |= true;
				}
				if ( foundPair && foundTriple )
				{
					break;
					// NOTE going through all of them is fine if we didn't find either or only one
				}
			}
			if ( foundPair )
			{
				pairCount++;
			}
			if ( foundTriple )
			{
				tripleCount++;
			}
		}
		System.out.println( here +" pairs "+ pairCount +" triples "+ tripleCount +" result "+ ( pairCount * tripleCount ) );
	}
}



























