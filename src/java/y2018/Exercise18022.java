
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Exercise18022
{

	public static void main( String args[] )
	{
		final String here = "e18022.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		get lines
		for line
			add to set (ignoring navigable set optimization to avoid repeated comparisons later)
		for key in set
			for key in set except current key
				for letter in key (to min; though I think that they're probably similarly sized)
					if keys except this letter match
						super break, declare victory
		*/
		/*	
		// -- 4tests
		fileLines.clear();
		fileLines.add( "aaaza" );
		fileLines.add( "abb33" );
		fileLines.add( "aab33" );
		fileLines.add( "aaafa" );
		// -- 4tests
		*/
		for ( String line : fileLines )
		{
			for ( String otherLine : fileLines )
			{
				if ( line.equals( otherLine )
					|| line.length() != otherLine.length() )
				{
					continue;
				}
				for ( int ind = line.length() -1; ind >= 0; ind-- )
				{
					String lineWithout = line.substring( 0, ind ) + line.substring( ind +1, line.length() );
					String otherWithout = otherLine.substring( 0, ind ) + otherLine.substring( ind +1, otherLine.length() );
					if ( lineWithout.equals( otherWithout ) )
					{
						System.out.println( here +"match: "+ lineWithout );
						return;
					}
				}
			}
		}
	}
}



























