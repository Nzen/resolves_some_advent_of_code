
import java.io.IOException;
import java.nio.file.*;
import java.util.*;

public class Exercise210102
{

	public static void main(
			String args[]
	) {
		final String here = "e210102.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		*/
		Exercise210102.findDepth( fileLines );
	}

	public static void findDepth(
			List<String> fileLines
	) {
		int currentSum = 0, previousDepthSum = 0, previousDepth = 0, ancientDepth = 0, qualifiedDrops = 0;
		for ( String line : fileLines )
		{
			if ( line.isEmpty() )
				continue;
			int currDepth = Integer.parseInt( line );
			currentSum = currDepth + previousDepth + ancientDepth;
			if ( ancientDepth != 0 && currentSum > previousDepthSum )
			{	qualifiedDrops += 1; System.out.print( "+" ); }
			System.out.println( "cs "+ currentSum +" ad"+ ancientDepth );
			previousDepthSum = currentSum;
			ancientDepth = previousDepth;
			previousDepth = currDepth;
		}
		System.out.println( "\tanswer "+ qualifiedDrops );
	}

}



























