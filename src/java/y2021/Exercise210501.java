
import java.io.IOException;
import java.nio.file.*;
import java.util.*;

public class Exercise210501
{

	public static void main(
			String args[]
	) {
		final String here = "e210501.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		for lines
		    parse as first and last points
		    if vertical or horizontal
		        save points
		    // diagonal are for later
		for values in map
		    add if total > 1
		print total
		*/
		new Exercise210501().findVents( fileLines );
	}


	private void findVents(
			List<String> fileLines
	) {
	    boolean testing = true;
	    Map<String, Integer> point_intersections = new HashMap<>();
		for ( String line : fileLines )
		{
			if ( line.isEmpty() )
				continue;
			Line computedLine = lineAsPointsAndDirection( line );
			if ( computedLine.direction != Direction.diagonal )
			{
    			for ( String point : computedLine.points )
    			{
    			    if ( testing )
        			    System.out.print( point +" | " );
        	        if ( point_intersections.containsKey( point ) )
        	            point_intersections.put( point, point_intersections.get( point ) +1 );
        	       else
        	            point_intersections.put( point, 1 );
    			}
			}
			if ( testing )
			    System.out.println( "\t"+ computedLine.direction.name() );
		}
		int totalIntersections = 0;
		for ( Integer linesThere : point_intersections.values() )
		    if ( linesThere > 1 )
		        totalIntersections++;
		System.out.println( "\t"+ totalIntersections );
	}


    private Line lineAsPointsAndDirection(
            String line
    ) {
        int firstInd = 0, secondInd = firstInd +1;
        int xxInd = 0, yyInd = xxInd +1;
        String comma = ",";
        String[] barePoints = line.split( " -> " );

        String[] xyFirst = barePoints[ firstInd ].split( comma );
        int xxFirst = Integer.parseInt( xyFirst[ xxInd ] );
        int yyFirst = Integer.parseInt( xyFirst[ yyInd ] );

        String[] xySecond = barePoints[ secondInd ].split( comma );
        int xxSecond = Integer.parseInt( xySecond[ xxInd ] );
        int yySecond = Integer.parseInt( xySecond[ yyInd ] );

        Line computedLine = new Line();
        Direction direction;

        if ( xxFirst == xxSecond )
        {
            computedLine.direction = Direction.horizontal;
            int yyGreater = yyFirst > yySecond ? yyFirst : yySecond;
            int yyLesser = yyFirst < yySecond ? yyFirst : yySecond;
            int distance = yyGreater - yyLesser;
            computedLine.points = new String[ distance +1 ];
            for ( int ind = 0; ind < distance +1; ind++ )
            {
                int yyVal = yyLesser + ind;
                String point = xyFirst[ xxInd ] +","+ Integer.toString( yyVal );
                computedLine.points[ ind ] = point;
            }
        }
        else if ( yyFirst == yySecond )
        {
            computedLine.direction = Direction.vertical;
            int xxGreater = xxFirst > xxSecond ? xxFirst : xxSecond;
            int xxLesser = xxFirst < xxSecond ? xxFirst : xxSecond;
            int distance = xxGreater - xxLesser;
            computedLine.points = new String[ distance +1 ];
            for ( int ind = 0; ind < distance +1; ind++ )
            {
                int xxVal = xxLesser + ind;
                String point = Integer.toString( xxVal ) +","+ xyFirst[ yyInd ];
                computedLine.points[ ind ] = point;
            }
        }
        else
        {
           computedLine.direction = Direction.diagonal;
        }

        return computedLine;
    }

    enum Direction { horizontal, vertical, diagonal };

    class Line
    {
        Direction direction;
        String[] points = {};
    }

}



