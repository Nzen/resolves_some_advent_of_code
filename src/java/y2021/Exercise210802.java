
import java.io.IOException;
import java.nio.file.*;
import java.util.*;

public class Exercise210802
{

	public static void main(
			String args[]
	) {
		final String here = "e210802.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -52
		*/
		new Exercise210802().countSegments( fileLines );
	}


	private void countSegments(
			List<String> fileLines
	) {
		int outputs = 0;
		for ( String line : fileLines )
		{
			if ( line.isEmpty() )
				continue;
			SegmentSolver expertSystem = new SegmentSolver( line );
			outputs += expertSystem.shownValue();
		}
		System.out.println( "\tanswer is "+ outputs );
	}


	class SegmentSolver
	{
		int value = -1;

		String notSet = "-";
		String centerTop = notSet;
		String centerCenter = notSet;
		String centerBottom = notSet;
		String leftTop = notSet;
		String rightTop = notSet;
		String leftBottom = notSet;
		String rightBottom = notSet;

		String segment0 = "";
		String segment1 = "";
		String segment2 = "";
		String segment3 = "";
		String segment4 = "";
		String segment5 = "";
		String segment6 = "";
		String segment7 = "";
		String segment8 = "";
		String segment9 = "";

		String[] letters = { "a", "b", "c", "d", "e", "f", "g" };

		Map<String, List<String>> char_inputs = new HashMap<>();
		Map<String, List<String>> missingChar_inputs = new HashMap<>();
		Map<Integer, List<String>> length_inputs = new HashMap<>();

		List<String> inputsToResolve = new ArrayList<>( 10 );
		List<String> termsToResolve = new ArrayList<>( 4 );


		SegmentSolver(
				String line
		) {
			String[] input_terms = line.split( " \\| " );
			int inputInd = 0, termInd = inputInd +1;
			String[] inputsSplit = input_terms[ inputInd ].split( " " );
			String[] termsSplit = input_terms[ termInd ].split( " " );
			for ( String input : inputsSplit )
			{
				char[] charsOfInput = input.toCharArray();
				Arrays.sort( charsOfInput );
				inputsToResolve.add( new String( charsOfInput ) );
			}
			for ( String term : termsSplit )
			{
				char[] charsOfTerm = term.toCharArray();
				Arrays.sort( charsOfTerm );
				termsToResolve.add( new String( charsOfTerm ) );
			}
		}


		int shownValue(
		) {
			characterizeByContent();
			characterizeByLength();
			deriveTopCenter();
			deriveTwoAndRightBottom();
			deriveRightTop();
			deriveSixAndFive();
			deriveLeftBottom();
			deriveZeroAndNine();
			deriveThree();
			translateTerms();

			return value;
		}


		private void characterizeByContent(
		) {
			for ( String letter : letters )
			{
				List<String> inputsWithLetter = new LinkedList<>();
				List<String> inputsWithoutLetter = new LinkedList<>();
				for ( String input : inputsToResolve )
				{
					if ( input.contains( letter ) )
						inputsWithLetter.add( input );
					else
						inputsWithoutLetter.add( input );
				}
				char_inputs.put( letter, inputsWithLetter );
				missingChar_inputs.put( letter, inputsWithoutLetter );
			}
		}


		private void characterizeByLength(
		) {
			for ( String input : inputsToResolve )
			{
				int len = input.length();
				if ( len == 2 )
					segment1 = input;
				else if ( len == 4 )
					segment4 = input;
				else if ( len == 3 )
					segment7 = input;
				else if ( len == 7 )
					segment8 = input;
				else
				{
					if ( ! length_inputs.containsKey( len ) )
						length_inputs.put( len, new LinkedList<>() );
					List<String> inputsOfLen = length_inputs.get( len );
					inputsOfLen.add( input );
				}
			}
		}


		private void deriveTopCenter(
		) {
			String inSevenNotOne = null;
			for ( int ind = 0; ind < segment7.length(); ind++ )
			{
				String there = segment7.substring( ind, ind +1 );
				if ( segment1.contains( there ) )
					continue;
				else
					inSevenNotOne = there;
			}
			if ( inSevenNotOne != null )
				centerTop = inSevenNotOne;
			else
				throw new RuntimeException( "could not derive tcenter" );
		}


		private void deriveTwoAndRightBottom(
		) {
			String bottomRight = null, two = null;
			for ( String candidate : missingChar_inputs.keySet() )
			{
				List<String> inputsThatLack = missingChar_inputs.get( candidate );
				if ( inputsThatLack.size() == 1 )
				{
					bottomRight = candidate;
					two = inputsThatLack.get( 0 );
					break;
				}
			}
			if ( bottomRight == null )
				throw new RuntimeException( "could not derive 2" );
			{
				segment2 = two;
				rightBottom = bottomRight;
			}
		}


		private void deriveRightTop(
		) {
			String firstChar = segment1.substring( 0, 1 );
			rightTop = ! rightBottom.equals( firstChar )
					? firstChar : segment1.substring( 1, 2 );
		}


		private void deriveSixAndFive(
		) {
			String six = null;
			List<String> inputsThatLackRightTop = missingChar_inputs.get( rightTop );
			if ( inputsThatLackRightTop.get( 0 ).length() == 5 )
			{
				segment6 = inputsThatLackRightTop.get( 1 );
				segment5 = inputsThatLackRightTop.get( 0 );
			}
			else
			{
				segment6 = inputsThatLackRightTop.get( 0 );
				segment5 = inputsThatLackRightTop.get( 1 );
			}
		}


		private void deriveLeftBottom(
		) {
			String bottomLeft = null;
			for ( int ind = 0; ind < segment6.length(); ind++ )
			{
				if ( ! segment5.contains( segment6.substring( ind, ind +1 ) ) )
				{
					bottomLeft = segment6.substring( ind, ind +1 );
					break;
				}
			}
			if ( bottomLeft == null )
				throw new RuntimeException( "could not derive lbottom" );
			else
				leftBottom = bottomLeft;
		}


		private void deriveZeroAndNine(
		) {
			List<String> inputsLen6 = length_inputs.get( 6 );
			for ( String input : inputsLen6 )
			{
				if ( input.equals( segment6 ) )
					continue;
				else if ( ! input.contains( leftBottom ) )
					segment9 = input;
				else
					segment0 = input;
			}
		}


		private void deriveThree(
		) {
			List<String> inputsLen5 = length_inputs.get( 5 );
			for ( String input : inputsLen5 )
			{
				if ( input.equals( segment2 ) || input.equals( segment5 ) )
					continue;
				else
				{
					segment3 = input;
					break;
				}
			}
		}


		private void translateTerms(
		) {
			int[] digits = { -1, -1, -1, -1 };
			for ( int ind = 0; ind < termsToResolve.size(); ind++ )
			{
				String term = termsToResolve.get( ind );
				if ( term.equals( segment0 ) )
					digits[ ind ] = 0;
				else if ( term.equals( segment1 ) )
					digits[ ind ] = 1;
				else if ( term.equals( segment2 ) )
					digits[ ind ] = 2;
				else if ( term.equals( segment3 ) )
					digits[ ind ] = 3;
				else if ( term.equals( segment4 ) )
					digits[ ind ] = 4;
				else if ( term.equals( segment5 ) )
					digits[ ind ] = 5;
				else if ( term.equals( segment6 ) )
					digits[ ind ] = 6;
				else if ( term.equals( segment7 ) )
					digits[ ind ] = 7;
				else if ( term.equals( segment8 ) )
					digits[ ind ] = 8;
				else if ( term.equals( segment9 ) )
					digits[ ind ] = 9;
			}
			value = 0;
			value += digits[ 0 ] * 1_000;
			value += digits[ 1 ] * 100;
			value += digits[ 2 ] * 10;
			value += digits[ 3 ];
		}


		/*


		private void (
		) {
		}
		*/


	}


}



























