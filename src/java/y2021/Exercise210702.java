
import java.io.IOException;
import java.nio.file.*;
import java.util.*;

public class Exercise210702
{

	public static void main(
			String args[]
	) {
		final String here = "e210702.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		for crab position
			increment number at that position
			update max
		for ( 1 - max position )
			for position
				increment cost * index
		*/
		Exercise210702.align( fileLines );
	}


	private static void align(
			List<String> fileLines
	) {
		String[] positionsStr = null;
		for ( String line : fileLines )
		{
			if ( line.isEmpty() )
				continue;
			positionsStr = line.split( "," );
		}
		boolean testing = true;

		int maxPosition = 0;
		Map<Integer, Integer> position_crabs = new HashMap<>();
		for ( String position : positionsStr )
		{
			int crabPosition = Integer.parseInt( position );
			if ( position_crabs.containsKey( crabPosition ) )
				position_crabs.put( crabPosition, position_crabs.get( crabPosition ) +1 );
			else
				position_crabs.put( crabPosition, Integer.valueOf( 1 ) );

			if ( crabPosition > maxPosition )
				maxPosition = crabPosition;
		}
		if ( testing )
		{
			for ( int ind = 0; ind <= maxPosition; ind++ )
			{
				if ( ind % 100 == 0 )
					System.out.println();
				if ( position_crabs.containsKey( ind )  )
					System.out.print( " "+ ind +"-"+ position_crabs.get( ind ) );
			}
			System.out.println();
		}

		long minimumDistance = (long)Integer.MAX_VALUE;
		// ASK slow ?
		for ( int center = 0; center <= maxPosition; center++ )
		{
			long currentTotalDistance = 0;
			for ( int ind = 0; ind <= maxPosition; ind++ )
			{
				if ( position_crabs.containsKey( ind )  )
				{
					int crabsHere = position_crabs.get( ind );
					currentTotalDistance += crabsHere * fuelCost( Math.abs( center - ind ) );
					if ( currentTotalDistance > minimumDistance )
						break; // ASK part 2 ?
				}
			}
			if ( currentTotalDistance < minimumDistance )
			{
				minimumDistance = currentTotalDistance;
				if ( testing )
					System.out.println( "cmin at "+ center +" is "+ minimumDistance );
			}
		}

		System.out.println( "\t"+ minimumDistance );
	}


	private static int fuelCost(
			int distance
	) {
		int total = 0;
		for ( int step = 1; step <= distance; step++ )
			total += step;
		return total;
	}


}



























