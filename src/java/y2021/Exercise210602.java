
import java.io.IOException;
import java.nio.file.*;
import java.util.*;

public class Exercise210602
{

	public static void main(
			String args[]
	) {
		final String here = "e210602.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		*/
		Exercise210602.simulateFish( fileLines );
	}


	private static void simulateFish(
			List<String> fileLines
	) {
		boolean testing = true;
		final int bornGenInd = 8, postBirthGenInd = 6;
		long[] generations = { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		for ( String line : fileLines )
		{
			if ( line.isEmpty() )
				continue;
			String[] initialPopulation = line.split( "," );
			for ( int ind = 0; ind < initialPopulation.length; ind++ )
			{
				int oneFishAge = Integer.parseInt( initialPopulation[ ind ] );
				generations[ oneFishAge ] += 1L;
			}
		}

		for ( int days = 256; days > 0; days-- )
		{
			long temp, tempForGenZero;
			temp = -1;
			tempForGenZero = generations[ 0 ];
			for ( int ind = 1; ind < generations.length; ind++ )
			{
				generations[ ind -1 ] = temp;
				temp = generations[ ind ];
				generations[ ind -1 ] = generations[ ind ];
			}
			generations[ bornGenInd ] = tempForGenZero;
			generations[ postBirthGenInd ] += tempForGenZero;
		}

		if ( testing )
			for ( int ind = 0; ind < generations.length; ind++ )
				System.out.println( "pop "+ ind +" is "+ generations[ ind ] );

		long finalPopulation = 0;
		for ( int ind = 0; ind < generations.length; ind++ )
			finalPopulation += generations[ ind ];
		System.out.println( "\tpopulation "+ finalPopulation );
	}


}



























