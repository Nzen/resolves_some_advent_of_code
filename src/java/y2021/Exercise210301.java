
import java.io.IOException;
import java.nio.file.*;
import java.util.*;

public class Exercise210301
{

	public static void main(
			String args[]
	) {
		final String here = "e210301.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		gamma is the more common bit, epsilon is the less common bit
		put these in a bit set and multiply the resulting binary number
		*/
		Exercise210301.interpretDiagnostic( fileLines );
	}


	private static void interpretDiagnostic(
			List<String> fileLines
	) {
		final int oneInd = 0, nilInd = oneInd +1;
		int[][] bitCounts = new int[ 2 ][ fileLines.get( 0 ).length() ];
		for ( int initInd = 0; initInd < bitCounts[ oneInd ].length; initInd++ )
		{
			bitCounts[ oneInd ][ initInd ] = 0;
			bitCounts[ nilInd ][ initInd ] = 0;
		}

		// sum bits
		for ( String line : fileLines )
		{
			if ( line.isEmpty() )
				continue;
			for ( int charInd = 0; charInd < line.length(); charInd++ )
			{
				if ( line.charAt( charInd ) == '1' )
					bitCounts[ oneInd ][ charInd ] += 1;
				else // == 0
					bitCounts[ nilInd ][ charInd ] += 1;
			}
		}

		// determine gamma and epsilon
		long gamma = 0L, epsilon = 0L, multiplier = 1;
		for ( int bitInd = bitCounts[ oneInd ].length -1; bitInd >= 0; bitInd-- )
		{
			if ( bitCounts[ oneInd ][ bitInd ] > bitCounts[ nilInd ][ bitInd ] )
				gamma += multiplier;
			else
				epsilon += multiplier;
			multiplier *= 2;
		}

		System.out.println( "\tg"+ gamma +" e"+ epsilon +" t"+ gamma * epsilon );
	}


}



























