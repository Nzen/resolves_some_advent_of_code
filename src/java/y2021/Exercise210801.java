
import java.io.IOException;
import java.nio.file.*;
import java.util.*;

public class Exercise210801
{

	public static void main(
			String args[]
	) {
		final String here = "e210801.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -52
		*/
		Exercise210801.countSegments( fileLines );
	}


	private static void countSegments(
			List<String> fileLines
	) {
		Collection<String> outputs = new LinkedList<>();
		for ( String line : fileLines )
		{
			if ( line.isEmpty() )
				continue;
			String[] minusDivider = line.split( " \\| " );
			String[] outputsOfLine = minusDivider[ 1 ].split( " " );
			for ( String output : outputsOfLine )
			{
				if( output.length() == 2 // 1
						|| output.length() == 4 // 4
						|| output.length() == 3 // 7
						|| output.length() == 7 ) // 8
					outputs.add( output );
			}
		}
		int segments = outputs.size();
		System.out.println( "\t"+ segments );
	}


}



























