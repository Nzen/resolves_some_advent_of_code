
import java.io.IOException;
import java.nio.file.*;
import java.util.*;

public class Exercise210502
{

	public static void main(
			String args[]
	) {
		final String here = "e210502.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		for lines
			parse as first and last points
			if vertical or horizontal
				save points
			// diagonal are for later
		for values in map
			add if total > 1
		print total
		*/
		new Exercise210502().findVents( fileLines );
	}


	private void findVents(
			List<String> fileLines
	) {
		boolean testing = false;
		Map<String, Integer> point_intersections = new HashMap<>();
		for ( String line : fileLines )
		{
			if ( line.isEmpty() )
				continue;
			Line computedLine = lineAsPointsAndDirection( line );
			//if ( computedLine.direction != Direction.diagonal )
				//continue;
			for ( String point : computedLine.points )
			{
				if ( testing )
					System.out.print( point +" | " );
				if ( point_intersections.containsKey( point ) )
					point_intersections.put( point, point_intersections.get( point ) +1 );
			else
					point_intersections.put( point, 1 );
			}
			if ( testing )
				System.out.println( "\t"+ computedLine.direction.name() );
		}
		int totalIntersections = 0;
		for ( Integer linesThere : point_intersections.values() )
			if ( linesThere > 1 )
				totalIntersections++;
		if ( testing )
			showPoints( point_intersections, 10 );
		System.out.println( "\ttotal "+ totalIntersections );
	}


	private Line lineAsPointsAndDirection(
			String line
	) {
		int firstInd = 0, secondInd = firstInd +1;
		int xxInd = 0, yyInd = xxInd +1;
		String comma = ",";
		String[] barePoints = line.split( " -> " );

		String[] xyFirst = barePoints[ firstInd ].split( comma );
		int xxFirst = Integer.parseInt( xyFirst[ xxInd ] );
		int yyFirst = Integer.parseInt( xyFirst[ yyInd ] );

		String[] xySecond = barePoints[ secondInd ].split( comma );
		int xxSecond = Integer.parseInt( xySecond[ xxInd ] );
		int yySecond = Integer.parseInt( xySecond[ yyInd ] );

		Line computedLine = new Line();

		if ( xxFirst == xxSecond )
		{
			computedLine.direction = Direction.horizontal;
			int yyGreater = yyFirst > yySecond ? yyFirst : yySecond;
			int yyLesser = yyFirst < yySecond ? yyFirst : yySecond;
			int distance = yyGreater - yyLesser;
			computedLine.points = new String[ distance +1 ];
			for ( int ind = 0; ind < distance +1; ind++ )
			{
				int yyVal = yyLesser + ind;
				String point = xyFirst[ xxInd ] +","+ Integer.toString( yyVal );
				computedLine.points[ ind ] = point;
			}
		}
		else if ( yyFirst == yySecond )
		{
			computedLine.direction = Direction.vertical;
			int xxGreater = xxFirst > xxSecond ? xxFirst : xxSecond;
			int xxLesser = xxFirst < xxSecond ? xxFirst : xxSecond;
			int distance = xxGreater - xxLesser;
			computedLine.points = new String[ distance +1 ];
			for ( int ind = 0; ind < distance +1; ind++ )
			{
				int xxVal = xxLesser + ind;
				String point = Integer.toString( xxVal ) +","+ xyFirst[ yyInd ];
				computedLine.points[ ind ] = point;
			}
		}
		else
		{
			computedLine.direction = Direction.diagonal;
			/*
			is \ when slope is negative 
			if \ then increment both
			if / then increment x and decrement y
			*/
			boolean firstIsLeftmost = xxFirst < xxSecond;
			boolean yyIncreases = ( firstIsLeftmost && yyFirst < yySecond )
					|| ( ! firstIsLeftmost && yyFirst > yySecond );
			int xxDistance = firstIsLeftmost
					? ( xxSecond - xxFirst ) : ( xxFirst - xxSecond );
			computedLine.points = new String[ xxDistance +1 ];
			int xxLeft, yyLeft, xxRight, yyRight;
			if ( firstIsLeftmost )
			{
				xxLeft = xxFirst;
				yyLeft = yyFirst;
				xxRight = xxSecond;
				yyRight = yySecond;
			}
			else
			{
				xxLeft = xxSecond;
				yyLeft = yySecond;
				xxRight = xxFirst;
				yyRight = yyFirst;
			}
			if ( yyIncreases )
			{
				for ( int ind = 0; ind < xxDistance +1; ind++ )
				{
					String point = Integer.toString( xxLeft + ind )
							+","+ Integer.toString( yyLeft + ind );
					computedLine.points[ ind ] = point;
		//System.out.println( " "+ point );
				}
			}
			else
			{
				for ( int ind = 0; ind < xxDistance +1; ind++ )
				{
					String point = Integer.toString( xxLeft + ind )
							+","+ Integer.toString( yyLeft - ind );
					computedLine.points[ ind ] = point;
		//System.out.println( " "+ point );
				}
			}
		}

		return computedLine;
	}
	
	
	private void showPoints(
		Map<String, Integer> point_intersections, int squareMagnitude
	) {
		String[][] board = new String[ squareMagnitude ][ squareMagnitude ];
		for ( int xx = 0; xx < squareMagnitude; xx++ )
			for ( int yy = 0; yy < squareMagnitude; yy++ )
				board[ xx ][ yy ] = " ";

		int xxInd = 0, yyInd = xxInd +1;
		String comma = ",";

		for ( String pointStr : point_intersections.keySet() )
		{
			String[] xyFirst = pointStr.split( comma );
			int xxFirst = Integer.parseInt( xyFirst[ xxInd ] );
			int yyFirst = Integer.parseInt( xyFirst[ yyInd ] );

			board[ xxFirst ][ yyFirst ] = Integer.toString( point_intersections.get( pointStr ) );
		}

		System.out.print( "\n | " );
		for ( int xx = 0; xx < squareMagnitude; xx++ )
			System.out.print( xx +" " );
		System.out.println();
		for ( int xx = 0; xx < squareMagnitude; xx++ )
		// for ( int xx = squareMagnitude -1; xx >= 0; xx-- )
		{
			System.out.print( xx +"|" );
			for ( int yy = 0; yy < squareMagnitude; yy++ )
				System.out.print( " "+ board[ yy ][ xx ] );
			System.out.println();
		}

	}

	enum Direction { horizontal, vertical, diagonal };

	class Line
	{
		Direction direction;
		String[] points = {};
	}

}



