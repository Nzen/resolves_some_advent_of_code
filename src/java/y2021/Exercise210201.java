
import java.io.IOException;
import java.nio.file.*;
import java.util.*;

public class Exercise210201
{

	public static void main(
			String args[]
	) {
		final String here = "e210201.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		*/
		Exercise210201.findFinalDistance( fileLines );
	}


	private static void findFinalDistance(
			List<String> fileLines
	) {
		final int dirInd = 0, valInd = dirInd +1;
		int horizontalPosition = 0, depth = 0;
		for ( String line : fileLines )
		{
			if ( line.isEmpty() )
				continue;
			String[] linePieces = line.split( " " );
			int magnitude = Integer.parseInt( linePieces[ valInd ] );
			switch ( linePieces[ dirInd ] )
			{
			case "forward" :
				horizontalPosition += magnitude;
				break;
			case "down" :
				depth += magnitude;
				break;
			case "up" :
				depth -= magnitude;
				break;
			default :
				continue;
			}
		}
		System.out.println( depth * horizontalPosition );
	}


}



























