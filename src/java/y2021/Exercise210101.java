
import java.io.IOException;
import java.nio.file.*;
import java.util.*;

public class Exercise210101
{

	public static void main(
			String args[]
	) {
		final String here = "e210101.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		*/
		Exercise210101.findDepth( fileLines );
	}

	public static void findDepth(
			List<String> fileLines
	) {
		int previousDepth = Integer.MAX_VALUE, drops = 0;
		for ( String line : fileLines )
		{
			if ( line.isEmpty() )
				continue;
			int currDepth = Integer.parseInt( line );
			if ( currDepth > previousDepth )
				drops += 1;
			previousDepth = currDepth;
		}
		System.out.println( drops );
	}

}



























