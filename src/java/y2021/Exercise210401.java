
import java.io.IOException;
import java.nio.file.*;
import java.util.*;

public class Exercise210401
{

	private BingoCell[][] bingoCells;
	private BingoCell[][] bingoCells2;
	private BingoCell[][] bingoCells3;


	public static void main(
			String args[]
	) {
		final String here = "e210401.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		save drawn numbers
		save boards
		for board
			calculate its time to win
			calculate the final score
		choose board with the least time to win
		*/
		new Exercise210401().winBingo( fileLines );
	}


	private void winBingo(
			List<String> fileLines
	) {
		// parse called
		String[] asciiCalled = fileLines.get( 0 ).split( "," );
		int[] called = new int[ asciiCalled.length ];
		for ( int callInd = 0; callInd < asciiCalled.length; callInd++ )
			called[ callInd ] = Integer.parseInt( asciiCalled[ callInd ] );
			

		// parse boards
		List<BingoBoard> boards = new LinkedList<>();
		for ( int lineInd = 2; lineInd < fileLines.size(); lineInd += 6 )
		{
			boards.add( new BingoBoard(
					fileLines.get( lineInd ),
					fileLines.get( lineInd +1 ),
					fileLines.get( lineInd +2 ),
					fileLines.get( lineInd +3 ),
					fileLines.get( lineInd +4 ) ) );
		}

		chooseBoard( boards, called );
	}


	private void chooseBoard(
			List<BingoBoard> boards, int[] called
	) {
		int minWinningTurns = Integer.MAX_VALUE;
		int winningBoardInd = -1;
		for ( int boardInd = 0; boardInd < boards.size(); boardInd++ )
		{
			BingoBoard board = boards.get( boardInd );
			calcTurnsToWinOrQuit( board, called, minWinningTurns );
			if ( board.turnsToWin < minWinningTurns && board.finalScore >= 0 )
			{
				minWinningTurns = board.turnsToWin;
				winningBoardInd = boardInd;
			}
		}
		System.out.println( "\t"+ boards.get( winningBoardInd ).finalScore );
	}


	private void calcTurnsToWinOrQuit(
			BingoBoard board, int[] called, int turnsToBeat
	) {
		for ( int turnInd = 0;
				turnInd < called.length && turnInd <= turnsToBeat;
				turnInd++ )
		{
			// mark cell, if applicable
			for ( int markRowInd = 0; markRowInd < board.board.length; markRowInd++ )
			{
				for ( int markColInd = 0; markColInd < board.board[ markRowInd ].length; markColInd++ )
				{
					if ( board.board[ markRowInd ][ markColInd ].value == called[ turnInd ] )
					{
						board.board[ markRowInd ][ markColInd ].called = true;
						// check if column won
						boolean everythingMarked = true;
						for ( int scoreRowInd = 0; scoreRowInd < board.board.length; scoreRowInd++ )
						{
							everythingMarked &= board.board[ scoreRowInd ][ markColInd ].called;
						}
						if ( everythingMarked )
						{
							board.calcFinalScore( markRowInd, markColInd );
							return;
						}
						else
						{
							// check if row won
							everythingMarked = true;
							for ( int scoreColInd = 0; scoreColInd < board.board.length; scoreColInd++ )
							{
								everythingMarked &= board.board[ markRowInd ][ scoreColInd ].called;
							}
							if ( everythingMarked )
							{
								board.calcFinalScore( markRowInd, markColInd );
								board.turnsToWin = turnInd;
								return;
							}
						}
					}
				}
			}
		}
	}


	class BingoBoard
	{

		final int boardWidth = 5;
		int turnsToWin = Integer.MAX_VALUE;
		int finalScore = -1;
		BingoCell[][] board = new BingoCell[ boardWidth ][ boardWidth ];


		public BingoBoard(
				String row0,
				String row1,
				String row2,
				String row3,
				String row4
		) {
			int rowInd = 0;
			fillRow( row0, rowInd );
			rowInd++;
			fillRow( row1, rowInd );
			rowInd++;
			fillRow( row2, rowInd );
			rowInd++;
			fillRow( row3, rowInd );
			rowInd++;
			fillRow( row4, rowInd );
			rowInd++;
		}


		private void fillRow(
				String rowValues, int rowInd
		) {
			String[] values = rowValues.split( " " );
			int cellInd = 0;
			for ( int valInd = 0; valInd < values.length; valInd++ )
			{
				if ( values[ valInd ].isEmpty() )
					continue;
				board[ rowInd ][ cellInd ] = new BingoCell( Integer.parseInt( values[ valInd ] ) );
				cellInd++;
			}
		}


		private void calcFinalScore(
				int lastRowInd, int lastColInd
		) {
			finalScore = 0;
			for ( int markRowInd = 0; markRowInd < board.length; markRowInd++ )
			{
				for ( int markColInd = 0; markColInd < board[ markRowInd ].length; markColInd++ )
				{
					if ( ! board[ markRowInd ][ markColInd ].called )
						finalScore += board[ markRowInd ][ markColInd ].value;
				}
			}
			finalScore *= board[ lastRowInd ][ lastColInd ].value;
		}

	}


	class BingoCell
	{
		int value;
		boolean called = false;


		public BingoCell(
				int adopt
		) {
			value = adopt;
		}

	}

}


















