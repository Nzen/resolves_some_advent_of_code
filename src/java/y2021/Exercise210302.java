
import java.io.IOException;
import java.nio.file.*;
import java.util.*;

public class Exercise210302
{

	public static void main(
			String args[]
	) {
		final String here = "e210302.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		sum the bits individually, discard and resum as necessary
		*/
		Exercise210302.interpretDiagnostic( fileLines );
	}


	private static void interpretDiagnostic(
			List<String> fileLines
	) {
		int ones = 0, nils = 0;
		int oxygenRating = 0, carbonRating = 0;
		List<String> previousCandidates = new ArrayList<>( fileLines.size() );
		previousCandidates.addAll( fileLines );
		List<String> currentCandidates = new LinkedList<>();
		int len = fileLines.get( 0 ).length();

		// oxygen
		for ( int charInd = 0; charInd < len; charInd++ )
		{
			ones = 0;
			nils = 0;
			for ( String line : previousCandidates )
			{
				if ( line.charAt( charInd ) == '1' )
					ones += 1;
				else // == 0
					nils += 1;
			}

			char toMatch;
			if ( ones > nils )
				toMatch = '1';
			else if ( ones < nils )
				toMatch = '0';
			else
				toMatch = '='; // for clarity when comparing == below
			for ( String candidate : previousCandidates )
			{
				if ( candidate.charAt( charInd ) == toMatch
						|| ( ones == nils
							&& candidate.charAt( charInd ) == '1' ) )
					currentCandidates.add( candidate );
			}
			if ( currentCandidates.size() == 1 )
				break;
			previousCandidates.clear();
			previousCandidates.addAll( currentCandidates );
			currentCandidates.clear();
		}
		oxygenRating = Exercise210302.asInt( currentCandidates.get( 0 ) );

		// carbon
		previousCandidates.clear();
		previousCandidates.addAll( fileLines );
		currentCandidates.clear();
		for ( int charInd = 0; charInd < len; charInd++ )
		{
			ones = 0;
			nils = 0;
			for ( String line : previousCandidates )
			{
				if ( line.charAt( charInd ) == '1' )
					ones += 1;
				else // == 0
					nils += 1;
			}

			char toMatch;
			if ( ones < nils )
				toMatch = '1';
			else if ( ones > nils )
				toMatch = '0';
			else
				toMatch = '='; // for clarity when comparing == below
			for ( String candidate : previousCandidates )
			{
				if ( candidate.charAt( charInd ) == toMatch
						|| ( ones == nils
							&& candidate.charAt( charInd ) == '0' ) )
					currentCandidates.add( candidate );
			}
			if ( currentCandidates.size() == 1 )
				break;
			previousCandidates.clear();
			previousCandidates.addAll( currentCandidates );
			currentCandidates.clear();
		}
		carbonRating = Exercise210302.asInt( currentCandidates.get( 0 ) );

		System.out.println( "\to"+ oxygenRating +" c"+ carbonRating +" t"+ oxygenRating * carbonRating );
	}


	private static int asInt(
			String bitChars
	) {
		int value = 0, multiplier = 1;
		for ( int charInd = bitChars.length() -1; charInd >= 0; charInd-- )
		{
			if ( bitChars.charAt( charInd ) == '1' )
				value += multiplier;
			multiplier *= 2;
		}
		return value;
	}


}



























