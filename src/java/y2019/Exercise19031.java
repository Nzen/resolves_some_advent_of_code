
import java.awt.Point;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

public class Exercise19031
{

	public static void main( String args[] )
	{
		final String here = Exercise19031.cl +"m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		input has two csv strings, each is a wire
		elements are cardinal vectors of operand {R D L U}
		note the paths these take
		note the intersections between them (but not of itself)
		of those intersections, choose the point closest to origin
		convert that distance to the 'manhattan'/'taxicab' distance (by making cardinal vectors to reach)
		this is the result to provide

		potential means
		make a set of integral points on the line of each part of the path
		for each point of first wire, check if second wire contains point
			if so add to intersection set
		calculate the distance of each point in the intersection set, directly or as 'manhattan' distance
		create a map of intersection points : 'manhattan' distance from the origin
		choose the lowest valued point, reply with this

		split by comma
		have an awareness of the current 
		convert instruction into a vector
		gen the list of point pairs indicated
		*/
		new Exercise19031( fileLines.get( 0 ), fileLines.get( 1 ) )
				.manhattanDistanceOfClosestIntersection();
	}


	private static final String cl = "e19031.";
	private String firstWire;
	private String secondWire;


	public Exercise19031( String first, String second )
	{
		firstWire = first;
		secondWire = second;
	}


	public void manhattanDistanceOfClosestIntersection()
	{
		final String here = cl +"mdhoci ";
		// System.out.println( "\t"+ here + "--- "+ firstWire );
		Collection<Vector> convertedFirst = converted( firstWire );
		Set<Cpoint> pointsFirst = pointsOfLine( convertedFirst );

		// System.out.println( "\t"+ here + "--- "+ secondWire );
		Collection<Vector> convertedSecond = converted( secondWire );
		Set<Cpoint> pointsSecond = pointsOfLine( convertedSecond );

		// render( pointsFirst, pointsSecond );
		Set<Cpoint> crossover = intersectionsOf( pointsFirst, pointsSecond );
		Cpoint closest = closestToOriginManhattanwise( crossover );
		System.out.println( "\t"+ here + "closest "+ closest );
		/*
		make a set of integral points on the line of each part of the path
		for each point of first wire, check if second wire contains point
			if so add to intersection set
		calculate the distance of each point in the intersection set, directly or as 'manhattan' distance
		create a map of intersection points : 'manhattan' distance from the origin
		choose the lowest valued point, reply with this

		have an awareness of the current 
		convert instruction into a vector
		gen the list of point pairs indicated
		*/
		// 4TESTS
	}


	private Collection<Vector> converted( String which )
	{
		String[] notCsv = which.split( "," );
		Collection<Vector> converted = new ArrayList<>( notCsv.length );
		for ( String acronym : notCsv )
		{
			converted.add( vectorOfAcronym( acronym ) );
		}
		return converted;
	}


	private Vector vectorOfAcronym( String input )
	{
		Vector result = new Vector();
		result.magnitude = Integer.parseInt( input.substring( 1 ) );
		result.direction = CardinalDirection.from( input.substring( 0, 1 ) );
		return result;
	}


	private Set<Cpoint> pointsOfLine( Collection<Vector> instructions )
	{
		final String here = cl +"pol ";
		// Set<Cpoint> allPoints = new HashSet<>( instructions.size() * 10 );
		Set<Cpoint> allPoints = new TreeSet<>();
		Cpoint origin = new Cpoint( 0, 0 );
		Cpoint previous = origin;
		Cpoint tip = origin;
		for ( Vector arrow : instructions )
		{
			// System.out.println( "\t\taa "+ arrow );
			switch ( arrow.direction )
			{
				case UP :
				{
					tip = new Cpoint( previous.x, previous.y + arrow.magnitude );
					// System.out.println( "\t\tt "+ tip.x +" : "+ tip.y );
					allPoints.add( tip );
					for ( int ind = previous.y; ind < tip.y; ind++ )
					{
						allPoints.add( new Cpoint( previous.x, ind ) );
						// System.out.println( "\t\t"+ previous.x +" : "+ ind );
					}
					break;
				}
				case DOWN :
				{
					tip = new Cpoint( previous.x, previous.y - arrow.magnitude );
					// System.out.println( "\t\tt "+ tip.x +" : "+ tip.y );
					allPoints.add( tip );
					for ( int ind = tip.y; ind < previous.y; ind++ )
					{
						allPoints.add( new Cpoint( previous.x, ind ) );
						// System.out.println( "\t\t"+ previous.x +" : "+ ind );
					}
					break;
				}
				case LEFT :
				{
					tip = new Cpoint( previous.x - arrow.magnitude, previous.y );
					// System.out.println( "\t\tt "+ tip.x +" : "+ tip.y );
					allPoints.add( tip );
					for ( int ind = tip.x; ind < previous.x; ind++ )
					{
						allPoints.add( new Cpoint( ind, previous.y ) );
						// System.out.println( "\t\t"+ ind +" : "+ previous.y );
					}
					break;
				}
				case RIGHT :
				{
					tip = new Cpoint( previous.x + arrow.magnitude, previous.y );
					// System.out.println( "\t\tt "+ tip.x +" : "+ tip.y );
					allPoints.add( tip );
					for ( int ind = previous.x; ind < tip.x; ind++ )
					{
						allPoints.add( new Cpoint( ind, previous.y ) );
						// System.out.println( "\t\t"+ ind +" : "+ previous.y );
					}
					break;
				}
				default :
				{
					break;
				}
			}
			// System.out.println( "\t"+ here + tip );
			previous.setLocation( tip );
		}
		/*
		for ( Cpoint buh : allPoints )
		{
			System.out.println( "\t"+ here + buh );
		}
		*/
		return allPoints;
	}


	private void render( Set<Cpoint> toShow )
	{
		int minXx = 0;
		int maxXx = 0;
		int minYy = 0;
		int maxYy = 0;
		for ( Cpoint candidate : toShow )
		{
			if ( candidate.x < minXx )
				minXx = candidate.x;
			if ( candidate.x > maxXx )
				maxXx = candidate.x;
			if ( candidate.y < minYy )
				minYy = candidate.y;
			if ( candidate.y > maxYy )
				maxYy = candidate.y;
		}
		Cpoint probe = new Cpoint( 0, 0 );
		for ( int indXx = minXx; indXx <= maxXx; indXx += 1 )
		{
			for ( int indYy = minYy; indYy <= maxYy; indYy += 1 )
			{
				probe.setLocation( indXx, indYy );
				if ( toShow.contains( probe ) )
					System.out.print( "x " );
				else
					System.out.print( "- " );
			}
			System.out.println();
		}
	}


	private void render( Set<Cpoint> first, Set<Cpoint> second )
	{
		int minXx = 0;
		int maxXx = 0;
		int minYy = 0;
		int maxYy = 0;
		for ( Cpoint candidate : first )
		{
			if ( candidate.x < minXx )
				minXx = candidate.x;
			if ( candidate.x > maxXx )
				maxXx = candidate.x;
			if ( candidate.y < minYy )
				minYy = candidate.y;
			if ( candidate.y > maxYy )
				maxYy = candidate.y;
		}
		for ( Cpoint candidate : second )
		{
			if ( candidate.x < minXx )
				minXx = candidate.x;
			if ( candidate.x > maxXx )
				maxXx = candidate.x;
			if ( candidate.y < minYy )
				minYy = candidate.y;
			if ( candidate.y > maxYy )
				maxYy = candidate.y;
		}
		Cpoint probe = new Cpoint( minXx, minYy );
		for ( int indXx = minXx; indXx <= maxXx; indXx += 1 )
		{
			for ( int indYy = minYy; indYy <= maxYy; indYy += 1 )
			{
				probe.setLocation( indXx, indYy );
				if ( first.contains( probe ) && second.contains( probe ) )
					System.out.print( "* " );
				else if ( first.contains( probe ) )
					System.out.print( "x " );
				else if ( second.contains( probe ) )
					System.out.print( "k " );
				else
					System.out.print( "- " );
			}
			System.out.println();
		}
	}


	private Set<Cpoint> intersectionsOf( Set<Cpoint> first, Set<Cpoint> second )
	{
		final String here = cl +"io ";
		Set<Cpoint> intersections = new HashSet<>( first.size() /10 );
		for ( Cpoint candidate : first )
		{
			if ( second.contains( candidate ) )
			{
				intersections.add( candidate );
				// System.out.println( here + candidate );
			}
		}
		return intersections;
	}


	// deprecated
	private Cpoint closestToOrigin( Set<Cpoint> somePoints )
	{
		Cpoint origin = new Cpoint( 0, 0 );
		Cpoint probe = new Cpoint( 0, 0 );
		double minDistance = Double.MAX_VALUE, currDistance = -1;
		for ( Cpoint candidate : somePoints )
		{
			if ( candidate.compareTo( origin ) == 0 )
				continue;
			currDistance = origin.distance( candidate );
			if ( currDistance < minDistance )
			{
				probe.setLocation( candidate );
				minDistance = currDistance;
			}
		}
		System.out.println( cl +"coo "+ minDistance );
		return probe;
	}


	private Cpoint closestToOriginManhattanwise( Set<Cpoint> somePoints )
	{
		Cpoint origin = new Cpoint( 0, 0 );
		Cpoint probe = new Cpoint( 0, 0 );
		int minDistance = Integer.MAX_VALUE, currDistance = -1;
		for ( Cpoint candidate : somePoints )
		{
			if ( candidate.compareTo( origin ) == 0 )
				continue;
			currDistance = Math.abs( candidate.x ) + Math.abs( candidate.y );
			if ( currDistance < minDistance )
			{
				probe.setLocation( candidate );
				minDistance = currDistance;
			}
		}
		System.out.println( cl +"coo "+ minDistance );
		return probe;
	}


	private class Vector
	{
		int magnitude = 0;
		CardinalDirection direction = CardinalDirection.UNKNOWN;

		@Override
		public String toString()
		{
			return direction.toString() + Integer.valueOf( magnitude ).toString();
		}
	}


	private enum CardinalDirection
	{
		UP( "U" ),
		DOWN( "D" ),
		LEFT( "L" ),
		RIGHT( "R" ),
		UNKNOWN( "" );

		public static CardinalDirection from( String candidate )
		{
			for ( CardinalDirection canon : CardinalDirection.values() )
			{
				if ( canon.toString().equals( candidate ) )
					return canon;
			}
			return CardinalDirection.UNKNOWN; // NOTE fallback
		}

		private String equivalent;

		private CardinalDirection( String code )
		{
			equivalent = code;
		}

		@Override
		public String toString()
		{
			return equivalent;
		}
	}


	private class Cpoint extends Point implements Comparable<Cpoint>
	{
		public Cpoint( int xx, int yy )
		{
			super( xx, yy );
		}

		@Override
		public int compareTo( Cpoint another )
		{
			if ( this.x == another.x )
			{
				if ( this.y == another.y )
					return 0;
				else if ( this.y < another.y )
					return -1;
				else
					return 1;
			}
			else if ( this.x < another.x )
				return -1;
			else
				return 1;
		}

		@Override
		public String toString()
		{
			return "C"+ x +","+ y;
		}
	}

}



























