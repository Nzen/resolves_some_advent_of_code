
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Exercise19011
{

	public static void main( String args[] )
	{
		final String here = "e19011.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		for values in file
			total += floor(x / 3) -2
		*/
		long total = 0;
		int lineNum = 0;
		for ( String line : fileLines )
		{
			lineNum += 1;
			int moduleWeight = 0;
			try
			{
				moduleWeight = Integer.parseInt( line );
			}
			catch ( NumberFormatException nfe )
			{
				System.err.println( here +"handed a non int "+ line +" on "+ lineNum +" because "+ nfe );
				continue;
			}
			total += Math.floor( moduleWeight / 3 ) -2;
		}
		System.out.println( here +"total is "+ total );
	}
}



























