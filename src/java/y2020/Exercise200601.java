
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Exercise200601
{

	public static void main( String args[] )
	{
		final String here = "e20011.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		*/
		// gather
		Set<Character> groupAnswers = new HashSet<>();
		int target = 0, lineNum = 0;
		for ( String line : fileLines )
		{
			if ( line.isEmpty() )
			{
				lineNum += 1;
				target += groupAnswers.size();
				// presumably, the answers will matter in the next session
				groupAnswers.clear();
			}
			else
			{
				for ( int ind = 0; ind < line.length(); ind++ )
					groupAnswers.add( line.charAt( ind ) );
			}
		}
		if ( ! groupAnswers.isEmpty() )
			target += groupAnswers.size();
		System.out.println( here +"input had no pair that summed to "+ target );
	}
}



























