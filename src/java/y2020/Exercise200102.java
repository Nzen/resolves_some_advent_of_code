
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Exercise200102
{

	public static void main( String args[] )
	{
		final String here = "e20012.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		for values in file
			save
		for values in less than half
			for values in more than half
				if sum is target
					print less * more
		*/
		// gather
		Set<Integer> values = new HashSet<>();
		Integer temp;
		int target = 2020;
		int lineNum = 0;
		for ( String line : fileLines )
		{
			lineNum += 1;
			try
			{
				temp = Integer.parseInt( line );
				if ( values.contains( temp ) )
				{
					System.err.println( here +"input contains duplicates, you have to handle that now" );
					return;
				}
				else
					values.add( temp );
			}
			catch ( NumberFormatException nfe )
			{
				System.err.println( here +"handed a non int "+ line +" on "+ lineNum +" because "+ nfe );
				continue;
			}
		}
		// combinatoric
		for ( Integer uno : values )
		{
			for ( Integer two : values )
			{
				if ( uno == two )
					continue;
				for ( Integer san : values )
				{
					if ( uno == san || two == san )
						continue;
					else if ( (uno + two + san) == target )
					{
						System.err.println( here +"one is "+ uno +" nee is "+ two
								+" trs is "+ san +" product is "+ (uno * two * san) );
						return;
					}
				}
			}
		}
		System.out.println( here +"input had no pair that summed to "+ target );
	}
}



























