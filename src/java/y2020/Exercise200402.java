
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.*;
import java.util.Set;
import java.util.TreeMap;

public class Exercise200402
{

	public static void main( String args[] )
	{
		final String here = "e20042.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		*/
		int valid = 0;
		Collection<String> onePassport = new LinkedList<>();
		int lineNum = 0;
		boolean parsing = false;
		Exercise200402 buh = new Exercise200402();
		Pattern hexColor = Pattern.compile( "#[abcdef\\d]{6}" );
		for ( String line : fileLines )
		{
			if ( line.isEmpty() )
			{
				lineNum += 1;
				if ( ! onePassport.isEmpty() )
				{
					Exercise200402.Passport person = buh.new Passport( onePassport );
					if ( person.valid( hexColor ) )
					{
						valid += 1;
			System.out.println( "\t"+ person );
					}
				}
				onePassport.clear();
				continue;
			}
			else
			{
				onePassport.add( line );
			}
		}
		if ( ! onePassport.isEmpty() )
		{
			Exercise200402.Passport person = buh.new Passport( onePassport );
			if ( person.valid( hexColor ) )
			{
				valid += 1;
	System.out.println( "\t"+ person );
			}
		}
		System.out.println( here +"input summed to "+ valid +" of "+ lineNum +" total" );
	}


	private class Passport
	{
		String birthYear = "";
		String issueYear = "";
		String expirationYear = "";
		String height = "";
		String hairColor = "";
		String eyeColor = "";
		String passportId = "";
		String countryId = "";
		final String byr = "byr",
			iyr = "iyr",
			eyr = "eyr",
			hgt = "hgt",
			hcl = "hcl",
			ecl = "ecl",
			pid = "pid",
			cid = "cid";

		public Passport(
				Collection<String> input
		) {
			parse( input );
		}
		private void parse(
				Collection<String> input
		) {
			final String here = "e20012.pp.p ";
			for ( String line : input )
			{
				String[] lineElements = line.split( " " );
				for ( String element : lineElements )
				{
					int colonInd = element.indexOf( ':' );
					String aspect = element.substring( 0, colonInd );
					String content = element.substring( colonInd +1 );
					switch ( aspect )
					{
						case byr :
							birthYear = content;
							break;
						case iyr :
							issueYear = content;
							break;
						case eyr :
							expirationYear = content;
							break;
						case hgt :
							height = content;
							break;
						case hcl :
							hairColor = content;
							break;
						case ecl :
							eyeColor = content;
							break;
						case pid :
							passportId = content;
							break;
						case cid :
							countryId = content;
							break;
					}
				}
			}
		}
		public boolean valid(
				Pattern hexColor
		) {
			if ( birthYear.isEmpty()
					|| issueYear.isEmpty()
					|| expirationYear.isEmpty()
					|| height.isEmpty()
					|| hairColor.isEmpty()
					|| eyeColor.isEmpty()
					|| passportId.isEmpty() )
					// || countryId.isEmpty(); // haha, hacking
			{
				// System.out.println( "v not full" );
				return false;
			}
			else if ( ! clamp( birthYear, 1920, 2002 ) )
			{
				// System.out.println( "v not birthyear "+ birthYear );
				return false;
			}
			else if ( ! clamp( issueYear, 2010, 2020 ) )
			{
				// System.out.println( "v not issueYear "+ issueYear );
				return false;
			}
			else if ( ! clamp( expirationYear, 2020, 2030 ) )
			{
				// System.out.println( "v not expirationYear "+ expirationYear );
				return false;
			}
			else if ( ! eye() )
			{
				// System.out.println( "v not eye "+ eyeColor );
				return false;
			}
			else if ( ! height() )
			{
				// System.out.println( "v not eye "+ eyeColor );
				return false;
			}
			else if ( ! hexColor.matcher( hairColor ).find() )
			{
				// System.out.println( "v not hair matcher "+ hairColor );
				return false;
			}
			else if ( passportId.length() != 9 || ! clamp( passportId, 0, 999_999_999 ) )
			{
				// System.out.println( "v not passport "+ passportId );
				return false;
			}
			return true;
		}
		private boolean clamp(
				String asChars, int min, int max
		) {
			try {
				int value = Integer.parseInt( asChars );
				return value >= min && value <= max;
			}
			catch ( NumberFormatException nfe )
			{
				return false;
			}
		}
		private boolean eye(
		) {
			return eyeColor.equals( "amb" )
					|| eyeColor.equals( "blu" )
					|| eyeColor.equals( "brn" )
					|| eyeColor.equals( "gry" )
					|| eyeColor.equals( "grn" )
					|| eyeColor.equals( "hzl" )
					|| eyeColor.equals( "oth" );
		}
		private boolean height(
		) {
			if ( height.endsWith( "in" ) )
				return clamp( height.substring( 0, height.length() -2 ), 59, 76 );
			else if ( height.endsWith( "cm" ) )
				return clamp( height.substring( 0, height.length() -2 ), 150, 193 );
			else
				return false;
		}
		public String toString(
		) {
			return ""/*
				"by"+ birthYear
				+" iy"+ issueYear
				+"\t ey"+ expirationYear
				+"\t ec_"+ eyeColor
				*/
				+"\t h "+ height.substring( 0, height.length() -2 ) +"_"+ height.substring( height.length() -2 )
				/*
				+" pi"+ passportId
				+" _"+ hairColor
				*/ ;
		}
	}
}



























