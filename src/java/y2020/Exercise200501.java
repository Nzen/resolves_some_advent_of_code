
import java.awt.Point;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Exercise200501
{
	private static final char tree = '#', ground = '.';

	public static void main( String args[] )
	{
		final String here = "e20011.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		*/
		int validCount = 0, id = 0;
		for ( String instruction : fileLines )
		{
			id = seatId( instruction );
			if ( id > validCount )
				validCount = id;
		}
		System.out.println( here +"input has "+ validCount +" valid trees" );
	}


	private static int seatId(
			String instructions
	) {
		final String here = "e20051.m ";
		String rowInstructions = instructions.substring( 0, instructions.length() -3 );
		String columnInstructions = instructions.substring( instructions.length() -3 );
		int row = 0, lower = 0, upper = 127;
		for ( int ind = 0; ind < rowInstructions.length(); ind++ )
		{
			int range = upper - lower;
			int half = (int)Math.round( range / 2F );
			if ( rowInstructions.charAt( ind ) == 'F' )
				upper -= half;
			else
				lower += half;
		}
		row = lower;
		int column = 0;
		lower = 0; upper = 7;
		for ( int ind = 0; ind < columnInstructions.length(); ind++ )
		{
			int range = upper - lower;
			int half = (int)Math.round( range / 2F );
			if ( columnInstructions.charAt( ind ) == 'L' )
				upper -= half;
			else
				lower += half;
		}
		column = lower;
		return row * 8 + column;
	}



}



























