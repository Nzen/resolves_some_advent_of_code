
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Exercise200401
{

	public static void main( String args[] )
	{
		final String here = "e20012.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		for values in file
			save
		for values in less than half
			for values in more than half
				if sum is target
					print less * more
		*/
		// gather
		int valid = 0;
		Collection<Passport> values = new LinkedList<>();
		Collection<String> onePassport = new LinkedList<>();
		int lineNum = 0;
		boolean parsing = false;
		Exercise200401 buh = new Exercise200401();
		for ( String line : fileLines )
		{
			lineNum += 1;
			if ( line.isEmpty() )
			{
				if ( ! onePassport.isEmpty() )
				{
					Exercise200401.Passport person = buh.new Passport( onePassport );
					values.add( person );
					if ( person.valid() )
						valid += 1;
				}
				onePassport.clear();
				continue;
			}
			else
			{
				onePassport.add( line );
			}
		}
		System.out.println( here +"input summed to "+ valid );
	}


	private class Passport
	{
		String birthYear = "";
		String issueYear = "";
		String expirationYear = "";
		String height = "";
		String hairColor = "";
		String eyeColor = "";
		String passportId = "";
		String countryId = "";
		final String byr = "byr",
			iyr = "iyr",
			eyr = "eyr",
			hgt = "hgt",
			hcl = "hcl",
			ecl = "ecl",
			pid = "pid",
			cid = "cid";

		public Passport(
				Collection<String> input
		) {
			parse( input );
		}
		private void parse(
				Collection<String> input
		) {
			final String here = "e20012.pp.p ";
			for ( String line : input )
			{
				String[] lineElements = line.split( " " );
				for ( String element : lineElements )
				{
					int colonInd = element.indexOf( ':' );
					String aspect = element.substring( 0, colonInd );
					String content = element.substring( colonInd +1 );
					switch ( aspect )
					{
						case byr :
							birthYear = content;
							break;
						case iyr :
							issueYear = content;
							break;
						case eyr :
							expirationYear = content;
							break;
						case hgt :
							height = content;
							break;
						case hcl :
							hairColor = content;
							break;
						case ecl :
							eyeColor = content;
							break;
						case pid :
							passportId = content;
							break;
						case cid :
							countryId = content;
							break;
					}
				}
			}
		}
		public boolean valid(
		) {
			return ! birthYear.isEmpty()
					&& ! issueYear.isEmpty()
					&& ! expirationYear.isEmpty()
					&& ! height.isEmpty()
					&& ! hairColor.isEmpty()
					&& ! eyeColor.isEmpty()
					&& ! passportId.isEmpty();
					// && ! countryId.isEmpty(); // haha, hacking
		}
	}
}



























