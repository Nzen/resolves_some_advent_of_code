
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Exercise200602
{

	public static void main( String args[] )
	{
		final String here = "e20011.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		*/
		// gather
		Map<Character, Integer> groupAnswers = new TreeMap<>();
		int target = 0, lineNum = 0;
		for ( String line : fileLines )
		{
			if ( line.isEmpty() )
			{
				for ( Character answer : groupAnswers.keySet() )
					if ( groupAnswers.get( answer ) == lineNum )
						target += 1;
				groupAnswers.clear();
				lineNum = 0;
			}
			else
			{
				lineNum += 1;
				for ( int ind = 0; ind < line.length(); ind++ )
				{
					char answer = line.charAt( ind );
					if ( ! groupAnswers.containsKey( answer ) )
						groupAnswers.put( answer, 0 );
					groupAnswers.put( answer, groupAnswers.get( answer ) +1 );
				}
			}
		}
		if ( ! groupAnswers.isEmpty() )
		{
			for ( Character answer : groupAnswers.keySet() )
				if ( groupAnswers.get( answer ) == lineNum )
					target += 1;
		}
		System.out.println( here +"input had no pair that summed to "+ target );
	}
}



























