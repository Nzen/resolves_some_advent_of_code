
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Exercise200201
{

	public static void main( String args[] )
	{
		final String here = "e20011.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		for values in file
			parse line : value range, letter, password
			if letter has occurances in range in password
				increment
		*/
		int lineNum = 0;
		int validCount = 0;
		for ( String line : fileLines )
		{
			lineNum += 1;
			try
			{
				Exercise200201 passwordRule = new Exercise200201();
				// System.out.println( here +"checking "+ line );
				if ( passwordRule.satisfied( line ) )
					validCount += 1;
			}
			catch ( Exception nfe )
			{
				System.err.println( here +"handed a non int "+ line +" on "+ lineNum +" because "+ nfe );
				return;
			}
		}
		System.out.println( here +"input has "+ validCount +" valid passwords" );
	}


	private int minCount = 0;
	private int maxCount = 0;
	private String essentialChar = "";

	/** form of min hyphen max space letter colon space input */
	boolean satisfied( String entireLine )
	{
		final String here = "e20011.s ";
		// if invalid etc
		final int rangeInd = 0, interestInd = rangeInd +1, inputInd = interestInd +1;
		String[] pieces = entireLine.split( " " );
		String[] rangePieces = pieces[ rangeInd ].split( "-" );
		String interest = pieces[ interestInd ].substring(
			0, pieces[ interestInd ].indexOf( ":" ) );
		final int rangeMinInd = 0, rangeMaxInd = rangeMinInd +1;
		minCount = Integer.parseInt( rangePieces[ rangeMinInd ] );
		maxCount = Integer.parseInt( rangePieces[ rangeMaxInd ] );
		int timesUsed = 0;
		int ind = pieces[ inputInd ].indexOf( interest, 0 );
		while ( ind < pieces[ inputInd ].length() && ind >= 0 )
		{
			ind += 1;
			timesUsed += 1;
			if ( timesUsed > maxCount )
				return false;
			// System.out.println( "\t"+ here +"another "+ ind +" currcount "+ timesUsed );
			ind = pieces[ inputInd ].indexOf( interest, ind );
		}

		return timesUsed >= minCount;
	}

}



























