
import java.io.IOException;
import java.nio.file.*;
import java.util.*;
import java.util.regex.*;;

public class Exercise200701
{

	public static void main( String args[] )
	{
		final String here = "e20071.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		*/
		// gather
		Map<String, Collection<String>> reverseAdjacency = new HashMap<>();
		for ( String line : fileLines )
		{
			if ( line.isEmpty() || line.contains( "no other" ) )
				continue;
			int firstBagsInd = line.indexOf( "bags" );
			String container = line.substring( 0, firstBagsInd -1 );
// System.out.println( here +"is "+ container );
			Collection<String> inside = containedIn( line.substring(
					firstBagsInd +"bags contain ".length() ) );
			for ( String inner : inside )
			{
				if ( ! reverseAdjacency.containsKey( inner ) )
					reverseAdjacency.put( inner, new HashSet<String>() );
				reverseAdjacency.get( inner ).add( container );
			}
		}
/* everything
for ( String inner : reverseAdjacency.keySet() )
{
	System.out.print( inner +";\t" );
	for ( String has : reverseAdjacency.get( inner ) )
		System.out.print( has +", " );
	System.out.println();
}
	System.out.println();
*/
		// trace adjacency
		Queue<String> toCheck = new LinkedList<>();
		int target = 0;
		Collection<String> containsGold = reverseAdjacency.get( "shiny gold" );
		if ( containsGold == null )
			throw new RuntimeException( here +"gold not contained by anything" );
// else
// System.out.println( "gold in "+ containsGold.size() );
		Set<String> outermost = new TreeSet<>();
		outermost.addAll( containsGold );
		toCheck.addAll( containsGold );
		while ( ! toCheck.isEmpty() )
		{
			String bag = toCheck.poll();
			if ( bag == null )
				continue;
			outermost.add( bag );
			Collection<String> containedBy = reverseAdjacency.get( bag );
// System.out.println( "huh "+ bag +" in "+ containedBy );
			if ( containedBy == null )
				continue;

/*
System.out.print( bag +" contained by: "+ containedBy.size() +" " );
for ( String inner : containedBy )
 System.out.print( "_"+ inner );
System.out.println( "; left-"+ toCheck.size() );
*/

			toCheck.addAll( containedBy );
		}
		System.out.println( here +"input summed to "+ outermost.size() );
	}


	/** expecting '\d+ value bag(s), ...' and not given a 'no other' line. */
	private static Collection<String> containedIn(
			String list
	) {
// System.out.print( "\trest is -"+ list );
		Collection<String> inside = new LinkedList<>();
		for (
				int ind = 0;
				ind < list.length() && ind >= 0;
				ind += 1
		) {
			// number
			ind = list.indexOf( ' ', ind ) +1;
			// color
			int end = list.indexOf( "bag", ind );
			String color = list.substring( ind, end -1 );
			inside.add( color );
			ind = list.indexOf( ", ", end ) +2;
//System.out.println( "color "+ color +"; pointing at "+ ind +" is_"+ list.charAt( ind ) );
			if ( ind == 1 )
				break; // added 2 above
			
		}
		/*
		light red bags contain 1 bright white bag, 2 muted yellow bags.
		bright white bags contain 1 shiny gold bag.
		muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
		*/
		return inside;
	}
}



























