
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Exercise200202
{

	public static void main( String args[] )
	{
		final String here = "e20011.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		for values in file
			parse line : position pair, letter, password
			if letter is in only one of the positions (one indexed)
				increment
		*/
		int lineNum = 0;
		int validCount = 0;
		for ( String line : fileLines )
		{
			lineNum += 1;
			try
			{
				Exercise200202 passwordRule = new Exercise200202();
		//System.out.println( here +"checking "+ line );
				if ( passwordRule.satisfied( line ) )
					validCount += 1;
			}
			catch ( Exception nfe )
			{
				System.err.println( here +"handed a non int "+ line +" on "+ lineNum +" because "+ nfe );
				return;
			}
		}
		System.out.println( here +"input has "+ validCount +" valid passwords" );
	}


	private int minCount = 0;
	private int maxCount = 0;
	private String essentialChar = "";

	/** form of min hyphen max space letter colon space input */
	boolean satisfied( String entireLine )
	{
		final String here = "e20011.s ";
		// if invalid etc
		final int positionInd = 0, interestInd = positionInd +1, inputInd = interestInd +1;
		String[] pieces = entireLine.split( " " );
		String[] positionPieces = pieces[ positionInd ].split( "-" );
		String interest = pieces[ interestInd ].substring(
			0, pieces[ interestInd ].indexOf( ":" ) );
		char desired = interest.charAt( 0 );
		final int positionMinInd = 0, positionMaxInd = positionMinInd +1;
		int firstInd = Integer.parseInt( positionPieces[ positionMinInd ] ) -1;
		int secondInd = Integer.parseInt( positionPieces[ positionMaxInd ] ) -1;
		int timesUsed = 0;
		if ( firstInd < pieces[ inputInd ].length()
				&& pieces[ inputInd ].charAt( firstInd ) == desired )
		{
			timesUsed += 1;
		}
		if ( secondInd < pieces[ inputInd ].length()
				&& pieces[ inputInd ].charAt( secondInd ) == desired )
		{
			timesUsed += 1;
		}
		return timesUsed == 1;
	}

}



























