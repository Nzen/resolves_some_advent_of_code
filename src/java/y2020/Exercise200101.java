
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Exercise200101
{

	public static void main( String args[] )
	{
		final String here = "e20011.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		for values in file
			partition into more and less than half of target
		for values in less than half
			for values in more than half
				if sum is target
					print less * more
		*/
		// gather
		Set<Integer> lessThanHalf = new HashSet<>();
		Set<Integer> moreThanHalf = new HashSet<>();
		Integer temp;
		int target = 2020, half = target /2;
		boolean seenExactlyHalfAlready = false;
		int lineNum = 0;
		for ( String line : fileLines )
		{
			lineNum += 1;
			try
			{
				temp = Integer.parseInt( line );
				if ( temp < half )
					lessThanHalf.add( temp );
				else if ( temp > half )
					moreThanHalf.add( temp );
				else if ( ! seenExactlyHalfAlready )
					seenExactlyHalfAlready = true; // no need to save it
				else
				{
					System.err.println( here +"exactly half twice, squared is "+ (temp * temp) );
					return;
				}
			}
			catch ( NumberFormatException nfe )
			{
				System.err.println( here +"handed a non int "+ line +" on "+ lineNum +" because "+ nfe );
				continue;
			}
		}
		// combinatoric
		for ( Integer lesser : lessThanHalf )
		{
			for ( Integer greater : moreThanHalf )
			{
				if ( (lesser + greater) == target )
				{
					System.err.println( here +"lesser is "+ lesser +" greater is "+ greater +" product is "+ (lesser * greater) );
					return;
				}
			}
		}
		System.out.println( here +"input had no pair that summed to "+ target );
	}
}



























