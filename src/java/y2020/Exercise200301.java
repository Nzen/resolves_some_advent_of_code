
import java.awt.Point;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Exercise200301
{
	private static final char tree = '#', ground = '.';

	public static void main( String args[] )
	{
		final String here = "e20011.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		*/
		Point slope = new Point( 3, 1 );
		int validCount = treesInPath( fileLines, slope );
		System.out.println( here +"input has "+ validCount +" valid trees" );
	}


	private static int treesInPath(
			List<String> map,
			Point slope
	) {
		final String here = "e20031.m ";
		int trees = 0, times = 500;
		for ( Point ind = new Point( 0, 0 );
				ind.y < map.size() && times > 0;
				ind = nextPosition( map, ind, slope ) )
		{
			char collideWith = map.get( ind.y ).charAt( ind.x );
			if ( collideWith == tree )
			{
				trees += 1;			
	System.out.println( here +"tree at "+ ind );
			}
			times--;
		}
if ( times == 0 )
	System.out.println( here +"quit by count, " );
		return trees;
	}


	private static Point nextPosition(
			List<String> map,
			Point ind,
			Point slope
	) {
		int nextY = ind.y + slope.y;
		int nextX = ind.x + slope.x;
		if ( nextX >= map.get( 0 ).length() )
		{
			nextX = nextX - map.get( 0 ).length();
		}
		ind.setLocation( nextX, nextY );
		return ind;
	}

}



























