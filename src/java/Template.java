
/* The authors of Solves-Some-Advent-of-Code release this file under cryptographic autonomy v1 license terms. */

import java.io.IOException;
import java.nio.file.*;
import java.util.*;

public class Exercise21001
{

	public static void main(
			String[] args
	) {
		final String here = "e21001.m ";
		if ( args.length < 1 )
		{
			throw new RuntimeException( here +"add a filename argument" );
		}
		String userSaysFile = args[ 0 ];
		List<String> fileLines = new LinkedList<>();
		try
		{
			Path where = Paths.get( userSaysFile );
			fileLines = Files.readAllLines( where );
		}
		catch ( IOException | InvalidPathException ie )
		{
			System.err.println( here +"couldn't read file "+ userSaysFile +" because "+ ie );
			return;
		}
		/*
		- interpretation of spec -
		*/
		Exercise21001.( fileLines );
	}


	private static void (
			List<String> fileLines
	) {
		for ( String line : fileLines )
		{
			if ( line.isEmpty() )
				continue;
		}
		System.out.println( "\t"+  );
	}


}




















