
/* The authors of Solves-Some-Advent-of-Code release this file under cryptographic autonomy v1 license terms. */

import java.io.IOException;
import java.nio.file.*;
import java.util.*;

fun main(
	arguments : Array<String>
) {
	val here = "m() "
	if ( arguments.isEmpty() ) {
		System.err.println( here +"needs filename argument" )
		return
	}
	var inputFilepath = arguments[ 0 ]
	var solver = Day1Part2( inputFilepath )
	solver.resolve()
}


class Day1Part2(
	var inputFilepath : String,
) {

	private var linesOfInputFile : MutableList<String>

	init {
		val here = "d1p1() "
		if ( inputFilepath.isEmpty() )
			throw RuntimeException( "$here add a filename argument" )

		try {
			val where = Paths.get( inputFilepath )
			linesOfInputFile = Files.readAllLines( where )
		}
		catch ( any : Exception ) {
			System.err.println( here +"couldn't read file "+ inputFilepath +" because "+ any )
			linesOfInputFile = mutableListOf<String>()
		}
	}


	fun resolve(
	) {
		val here = "d1p2.r "
		val calibrationValues = mutableListOf<Int>()
		for ( line in linesOfInputFile ) {
			if ( line.isEmpty() )
				continue

			System.out.println( "- $line -" )
			val wordsOfNumerals = listOf(
					"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", )
			val firstIndiciesOfNumerals = mutableListOf( -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, )
			val firstIndiciesOfWords = mutableListOf( -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, )
			val lastIndiciesOfNumerals = mutableListOf( -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, )
			val lastIndiciesOfWords = mutableListOf( -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, )
			for ( index in 0 .. 9 ) {
				val numeralToSearchFor = index.toString()
				firstIndiciesOfNumerals[ index ] = line.indexOf( numeralToSearchFor )
				lastIndiciesOfNumerals[ index ] = line.lastIndexOf( numeralToSearchFor )
				var wordToSearchFor = wordsOfNumerals[ index ]
				firstIndiciesOfWords[ index ] = line.indexOf( wordToSearchFor )
				lastIndiciesOfWords[ index ] = line.lastIndexOf( wordToSearchFor )
			}

			/*
			for ( iterationIndex in 0 .. 9 ) {
				var positionIndex = firstIndiciesOfNumerals[ iterationIndex ]
				if ( positionIndex != -1 )
					System.out.println( "charIndex "+ positionIndex +" has f n"+ iterationIndex )
				positionIndex = firstIndiciesOfWords[ iterationIndex ]
				if ( positionIndex != -1 )
					System.out.println( "charIndex "+ positionIndex +" has f wo"+ iterationIndex )
				positionIndex = lastIndiciesOfNumerals[ iterationIndex ]
				if ( positionIndex != -1 )
					System.out.println( "charIndex "+ positionIndex +" has l n"+ iterationIndex )
				positionIndex = lastIndiciesOfWords[ iterationIndex ]
				if ( positionIndex != -1 )
					System.out.println( "charIndex "+ positionIndex +" has l wo"+ iterationIndex )
			}
			*/

			var firstDigit = 0
			var secondDigit = 0
			var lowestIndex = Int.MAX_VALUE
			var highestIndex = Int.MIN_VALUE
			for ( index in 0 .. 9 ) {
				val firstIndexOfNumeral = firstIndiciesOfNumerals[ index ]
				val firstIndexOfWord = firstIndiciesOfWords[ index ]
				if ( firstIndexOfNumeral >= 0 && firstIndexOfNumeral < lowestIndex ) {
					firstDigit = index
					lowestIndex = firstIndexOfNumeral
				}
				if ( firstIndexOfWord >= 0 && firstIndexOfWord < lowestIndex ) {
					firstDigit = index
					lowestIndex = firstIndexOfWord
				}

				val lastIndexOfNumeral = lastIndiciesOfNumerals[ index ]
				val lastIndexOfWord = lastIndiciesOfWords[ index ]
				if ( lastIndexOfNumeral >= 0 && lastIndexOfNumeral > highestIndex ) {
					secondDigit = index
					highestIndex = lastIndexOfNumeral
				}
				if ( lastIndexOfWord >= 0 && lastIndexOfWord > highestIndex ) {
					secondDigit = index
					highestIndex = lastIndexOfWord
				}
			}

			if ( secondDigit == 0 )
					secondDigit = firstDigit

			val calibrationValue = ( firstDigit *10 ) + secondDigit
			calibrationValues.add( calibrationValue );
			System.out.println( "value "+ calibrationValue )
		}

		var sumOfCalibrationValues = 0
		for ( value in calibrationValues )
			sumOfCalibrationValues += value

		System.out.println( "\tsolution "+ sumOfCalibrationValues )
	}


}

