
/* The authors of Solves-Some-Advent-of-Code release this file under cryptographic autonomy v1 license terms. */

import java.io.IOException;
import java.nio.file.*;
import java.util.*;

fun main(
	arguments : Array<String>
) {
	val here = "m() "
	if ( arguments.isEmpty() ) {
		System.err.println( here +"needs filename argument" )
		return
	}
	var inputFilepath = arguments[ 0 ]
	var solver = Day1Part1( inputFilepath )
	solver.resolve()
}


class Day1Part1(
	var inputFilepath : String
) {

	private var linesOfInputFile : MutableList<String>

	init {
		val here = "d1p1() "
		if ( inputFilepath.isEmpty() )
			throw RuntimeException( "$here add a filename argument" )

		try {
			val where = Paths.get( inputFilepath )
			linesOfInputFile = Files.readAllLines( where )
		}
		catch ( any : Exception ) {
			System.err.println( here +"couldn't read file "+ inputFilepath +" because "+ any )
			linesOfInputFile = mutableListOf<String>()
		}
	}


	fun resolve(
	) {
		val here = "d1p1.r "
		val calibrationValues = mutableListOf<Int>()
		for ( line in linesOfInputFile ) {
			if ( line.isEmpty() )
				continue

			var firstDigit = 0
			var secondDigit = 0
			for ( index in 0 .. line.length -1 ) {
				val maybeDigit = line.get( index )
				try {
					val digitValue : Int
					if ( maybeDigit.isDigit() )
						digitValue = maybeDigit.digitToInt()
					else
						continue
					if ( firstDigit == 0 )
						firstDigit = digitValue
					secondDigit = digitValue
				}
				catch ( nfe : NumberFormatException ) {
					// ignored, plenty of letters in the input
				}
			}

			val calibrationValue = ( firstDigit *10 ) + secondDigit
			calibrationValues.add( calibrationValue );
		}

		var sumOfCalibrationValues = 0
		for ( value in calibrationValues )
			sumOfCalibrationValues += value

		System.out.println( "\tsolution "+ sumOfCalibrationValues )
	}


}

