
/* The authors of Solves-Some-Advent-of-Code release this file under cryptographic autonomy v1 license terms. */

import java.io.IOException;
import java.nio.file.*;
import java.util.*;

fun main(
	arguments : Array<String>
) {
	val here = "m() "
	if ( arguments.isEmpty() ) {
		System.err.println( here +"needs filename argument" )
		return
	}
	var inputFilepath = arguments[ 0 ]
	var solver = Day2Part1( inputFilepath )
	solver.resolve()
}


public class Day2Part1(
	var inputFilepath : String,
) {

	private var linesOfInputFile : MutableList<String>

	init {
		val here = "d2p1() "
		if ( inputFilepath.isEmpty() )
			throw RuntimeException( "$here add a filename argument" )

		try {
			val where = Paths.get( inputFilepath )
			linesOfInputFile = Files.readAllLines( where )
		}
		catch ( any : Exception ) {
			System.err.println( here +"couldn't read file "+ inputFilepath +" because "+ any )
			linesOfInputFile = mutableListOf<String>()
		}
	}


	fun resolve(
	) {
		val here = "d2p1.r "
		val maxRed = 12; val maxGreen = 13; val maxBlue = 14
		val redWord = "red"; val greenWord = "green"; val blueWord = "blue"
		var sumOfIds = 0
		game@ for ( line in linesOfInputFile ) {
			if ( line.isEmpty() )
				continue

			/*
			separate game id from reveals
			separate reveals of game
			separate colors from reveal
			if colors are lower than maxes, add id to list
			*/
			var gameAndReveals = line.split( ":" )

			var reveals = gameAndReveals[ 1 ].split( ";" )
			for ( reveal in reveals ) {
				var colors = reveal.split( "," )
				for ( colorAndCount in colors ) {
					if ( colorAndCount.isEmpty() )
						continue
					val colorWithCount = colorAndCount.trim().split( " " )
					val count = colorWithCount[ 0 ].toInt()
					val colorWord = colorWithCount[ 1 ]

					if ( ( redWord == colorWord && count > maxRed ) || ( greenWord == colorWord && count > maxGreen ) || ( blueWord == colorWord && count > maxBlue ) ) {
						continue@game
					}
				}
			}

			var gameAndId = gameAndReveals[ 0 ].split( " " )
			var gameId = gameAndId[ 1 ].toInt()
			sumOfIds += gameId
		}

		System.out.println( "\tsolution "+ sumOfIds )
	}


}













