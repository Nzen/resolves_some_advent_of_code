
/* The authors of Solves-Some-Advent-of-Code release this file under cryptographic autonomy v1 license terms. */

import java.io.IOException;
import java.nio.file.*;
import java.util.*;

fun main(
	arguments : Array<String>
) {
	val here = "m() "
	if ( arguments.isEmpty() ) {
		System.err.println( here +"needs filename argument" )
		return
	}
	var inputFilepath = arguments[ 0 ]
	var solver = Day2Part2( inputFilepath )
	solver.resolve()
}


public class Day2Part2(
	var inputFilepath : String,
) {

	private var linesOfInputFile : MutableList<String>

	init {
		val here = "d2p2() "
		if ( inputFilepath.isEmpty() )
			throw RuntimeException( "$here add a filename argument" )

		try {
			val where = Paths.get( inputFilepath )
			linesOfInputFile = Files.readAllLines( where )
		}
		catch ( any : Exception ) {
			System.err.println( here +"couldn't read file "+ inputFilepath +" because "+ any )
			linesOfInputFile = mutableListOf<String>()
		}
	}


	fun resolve(
	) {
		val here = "d2p2.r "
		val redWord = "red"; val greenWord = "green"; val blueWord = "blue"
		var sumOfPowers = 0
		for ( line in linesOfInputFile ) {
			if ( line.isEmpty() )
				continue

			/*
			separate game id from reveals
			separate reveals of game
			separate colors from reveal
			if colors are lower than maxes, add id to list
			*/
			var maxRed = 0; var maxGreen = 0; var maxBlue = 0
			var gameAndReveals = line.split( ":" )
			var reveals = gameAndReveals[ 1 ].split( ";" )
			for ( reveal in reveals ) {
				var colors = reveal.split( "," )
				for ( colorAndCount in colors ) {
					if ( colorAndCount.isEmpty() )
						continue
					val colorWithCount = colorAndCount.trim().split( " " )
					val count = colorWithCount[ 0 ].toInt()
					val colorWord = colorWithCount[ 1 ]
					//System.out.println( " color w$colorWord c$count" )
					if ( redWord == colorWord && count > maxRed )
						maxRed = count
					else if ( greenWord == colorWord && count > maxGreen )
						maxGreen = count
					else if ( blueWord == colorWord && count > maxBlue )
						maxBlue = count
				}
			}

			var power = maxBlue * maxGreen * maxRed
			sumOfPowers += power
		}

		System.out.println( "\tsolution "+ sumOfPowers )
	}


}













