
/* The authors of Solves-Some-Advent-of-Code release this file under cryptographic autonomy v1 license terms. */

import java.io.IOException;
import java.nio.file.*;
import java.util.*;

fun main(
	arguments : Array<String>
) {
	val here = "m() "
	if ( arguments.isEmpty() ) {
		System.err.println( here +"needs filename argument" )
		return
	}
	var inputFilepath = arguments[ 0 ]
	var solver = DayPart( inputFilepath )
	solver.resolve()
}


public class Day Part(
	var inputFilepath : String,
) {

	private var linesOfInputFile : MutableList<String>

	init {
		val here = "dp() "
		if ( inputFilepath.isEmpty() )
			throw RuntimeException( "$here add a filename argument" )

		try {
			val where = Paths.get( inputFilepath )
			linesOfInputFile = Files.readAllLines( where )
		}
		catch ( any : Exception ) {
			System.err.println( here +"couldn't read file "+ inputFilepath +" because "+ any )
			linesOfInputFile = mutableListOf<String>()
		}
	}


	fun resolve(
	) {
		val here = "e.r "
		for ( line in linesOfInputFile ) {
			if ( line.isEmpty() )
				continue
		}

		System.out.println( "\tsolution "+ 1 )
	}


}

