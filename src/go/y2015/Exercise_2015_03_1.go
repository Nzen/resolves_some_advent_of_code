package main
 
import (
	"bufio"
	"fmt"
	"log"
	"os"
)

type Coordinate struct {
	xx, yy int
}

// produces a new Coordinate that is offset by one from the current coordinate
func (coordinate Coordinate) InDirection( arrow rune ) Coordinate {
	if arrow == '>' {
		return Coordinate{ coordinate.xx +1, coordinate.yy }
	} else if arrow == '^' {
		return Coordinate{ coordinate.xx, coordinate.yy +1 }
	} else if arrow == 'v' {
		return Coordinate{ coordinate.xx, coordinate.yy -1 }
	} else if arrow == '<' {
		return Coordinate{ coordinate.xx -1, coordinate.yy }
	} else {
		return Coordinate{ -1_000_000, -1_000_000 }
	}
}
 
func main() {
	if len( os.Args ) < 2 {
		log.Fatal( "Error: need filepath argument" )
	}
	filePath := os.Args[ 1 ]
	fileLines := readLines( filePath )

	countVisits( fileLines )
}

func readLines( filepath string ) []string {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	var lines []string

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if scanner.Err() != nil {
		log.Fatal(scanner.Err())
	}
	return lines
}

func countVisits( fileLines []string ) {
	houseVisits := make( map[ Coordinate ]int )
	nextLocation := Coordinate{ 0, 0 }
	houseVisits[ nextLocation ] += 1
	for _, line := range fileLines {
		if len( line ) < 1 {
			continue
		}
		// fmt.Println( line )
		for _, character := range line {
			nextLocation = nextLocation.InDirection( character )
			if nextLocation.xx == -1_000_000 && nextLocation.yy == -1_000_000 {
				fmt.Println( "paranoid, invalid direction" )
				return
			}
			houseVisits[ nextLocation ] += 1
		}
	}
	
	fmt.Println( len( houseVisits ) )
}

















