package main
 
import (
	"bufio"
	"fmt"
	"log"
	"os"
	"unicode/utf8"
)
 
func main() {
	if len( os.Args ) < 2 {
		log.Fatal( "Error: need filepath argument" )
	}
	filePath := os.Args[ 1 ]
	fileLines := readLines( filePath )

	classify( fileLines )
}

func readLines( filepath string ) []string {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	var lines []string

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if scanner.Err() != nil {
		log.Fatal(scanner.Err())
	}
	return lines
}

func classify( fileLines []string ) {
	nice := 0
	avoid := []string { "ab", "cd", "pq", "xy" }
	vowels := []rune { 'a', 'e', 'i', 'o', 'u' }

	thisWord :
	for _, line := range fileLines {
		if len( line ) < 1 {
			continue
		}
		// fmt.Println( line )

		hasSatisfiedDoubleLetter := false
		vowelsOfThisWord := 0
		previousLetter := '-'

		for _, character := range line {
			if vowelsOfThisWord < 3 {
				for _, vowel := range vowels {
					if vowel == character {
						vowelsOfThisWord += 1
						// fmt.Println( "\tvowel" )
						break
					}
				}
			}

			if previousLetter == character {
				hasSatisfiedDoubleLetter = true // ASK hasSatisfiedDoubleLetter |= invalid ?
				// fmt.Println( "found repeated pair" )
			}

			for _, pair := range avoid {
				firstRune, _ := utf8.DecodeRuneInString( pair[0:] )
				// fmt.Println( firstRune )
				if firstRune == previousLetter {
					secondRune, _ := utf8.DecodeRuneInString( pair[1:] )
					if secondRune == character {
						// fmt.Println( "found forbidden pair" )
						continue thisWord
					}
				}
			}

			previousLetter = character
		}

		if hasSatisfiedDoubleLetter && vowelsOfThisWord > 2 {
			nice += 1
			// fmt.Println( "\t"+ line )
		}
	}
	
	fmt.Println( nice )
}

















