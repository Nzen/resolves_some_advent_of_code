package main
 
import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)
 
func main() {
	if len( os.Args ) < 2 {
		log.Fatal( "Error: need filepath argument" )
	}
	filePath := os.Args[ 1 ]
	fileLines := readLines( filePath )

	calculateWrappingPaperSurfaceArea( fileLines )
}

func readLines( filepath string ) []string {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	var lines []string

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if scanner.Err() != nil {
		log.Fatal(scanner.Err())
	}
	return lines
}

func calculateWrappingPaperSurfaceArea( fileLines []string ) {
	lengthIndex := 0
	widthIndex := lengthIndex +1
	heightIndex := widthIndex +1
	var eachPresent []int
	for _, line := range fileLines {
		if len( line ) < 1 {
			continue
		}
		// fmt.Println( line )
		lengthWidthHeight := strings.Split( line, "x" )

		length, error := strconv.Atoi( lengthWidthHeight[ lengthIndex ] )
		if error != nil {
			log.Fatal( "not an int ", lengthWidthHeight[ lengthIndex ] )
		}
		width, error := strconv.Atoi( lengthWidthHeight[ widthIndex ] )
		if error != nil {
			log.Fatal( "not an int ", lengthWidthHeight[ widthIndex ] )
		}
		height, error := strconv.Atoi( lengthWidthHeight[ heightIndex ] )
		if error != nil {
			log.Fatal( "not an int ", lengthWidthHeight[ heightIndex ] )
		}

		lengthWidthSurfaceArea := length * width
		lengthHeightSurfaceArea := length * height
		widthHeightSurfaceArea := width * height

		minimumFace := 0
		if lengthWidthSurfaceArea <= lengthHeightSurfaceArea {
			if lengthWidthSurfaceArea <= widthHeightSurfaceArea {
				minimumFace = lengthWidthSurfaceArea
			} else {
				minimumFace = widthHeightSurfaceArea
			}
		} else if lengthHeightSurfaceArea <= widthHeightSurfaceArea {
			minimumFace = lengthHeightSurfaceArea
		} else {
			minimumFace = widthHeightSurfaceArea
		}

		basicSurfaceArea := lengthWidthSurfaceArea * 2 +
				lengthHeightSurfaceArea * 2 +
				widthHeightSurfaceArea * 2
		currentPresentWrappingPaperArea := basicSurfaceArea + minimumFace
		eachPresent = append( eachPresent, currentPresentWrappingPaperArea )
	}
	
	totalPresentWrappingPaperArea := 0
	for _, wrappingPaperArea := range eachPresent {
		totalPresentWrappingPaperArea += wrappingPaperArea
	}

	fmt.Println( totalPresentWrappingPaperArea )
}

















