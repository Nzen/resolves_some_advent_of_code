package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Error: need filepath argument")
	}
	filePath := os.Args[1]
	fileLines := readLines(filePath)

	classify(fileLines)
}

func readLines(filepath string) []string {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	var lines []string

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if scanner.Err() != nil {
		log.Fatal(scanner.Err())
	}
	return lines
}

func classify(fileLines []string) {
	nice := 0

	for _, line := range fileLines {
		if len(line) < 1 {
			continue
		}
		// fmt.Println( "testing "+ line )

		letterPairIndex := make(map[string]int)
		hasSatisfiedSandwich := false
		hasSatisfiedRepeatedPair := false
		beforeLastLetter := '#'
		lastLetter := '-'

		for letterIndex, character := range line {
			if !hasSatisfiedSandwich &&
					beforeLastLetter == character {
				hasSatisfiedSandwich = true
			}

			if !hasSatisfiedRepeatedPair {
				currentPair := string([]rune{lastLetter, character})
				previouslySeenIndexOfPair, found := letterPairIndex[currentPair]
				if ! found {
					letterPairIndex[currentPair] = letterIndex
					// fmt.Printf( "saving pair %s at %d\n", currentPair, letterIndex )
				} else if letterIndex > previouslySeenIndexOfPair+1 {
					hasSatisfiedRepeatedPair = true
					// fmt.Printf( "repeated pair %s at %d and %d\n", currentPair, letterIndex, previouslySeenIndexOfPair )

				} /* else {
					fmt.Printf( "ignoring overlap pair %s at %d (already %d)\n", currentPair, letterIndex, previouslySeenIndexOfPair )
				} */
			}

			if hasSatisfiedSandwich && hasSatisfiedRepeatedPair {
				nice += 1
				// fmt.Println( "\tnice" )
				break
			}

			beforeLastLetter = lastLetter
			lastLetter = character
		}
	}

	fmt.Println(nice)
}
