package main
 
import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)
 
func main() {
	if len( os.Args ) < 2 {
		log.Fatal( "Error: need filepath argument" )
	}
	filePath := os.Args[ 1 ]
	fileLines := readLines( filePath )

	calculateWrappingPaperSurfaceArea( fileLines )
}

func readLines( filepath string ) []string {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	var lines []string

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if scanner.Err() != nil {
		log.Fatal(scanner.Err())
	}
	return lines
}

func calculateWrappingPaperSurfaceArea( fileLines []string ) {
	lengthIndex := 0
	widthIndex := lengthIndex +1
	heightIndex := widthIndex +1
	lengthOfRibbonAsFeet := 0
	for _, line := range fileLines {
		if len( line ) < 1 {
			continue
		}
		// fmt.Println( line )
		lengthWidthHeight := strings.Split( line, "x" )

		length, error := strconv.Atoi( lengthWidthHeight[ lengthIndex ] )
		if error != nil {
			log.Fatal( "not an int ", lengthWidthHeight[ lengthIndex ] )
		}
		width, error := strconv.Atoi( lengthWidthHeight[ widthIndex ] )
		if error != nil {
			log.Fatal( "not an int ", lengthWidthHeight[ widthIndex ] )
		}
		height, error := strconv.Atoi( lengthWidthHeight[ heightIndex ] )
		if error != nil {
			log.Fatal( "not an int ", lengthWidthHeight[ heightIndex ] )
		}

		volume := length * width * height
		twiceLength := length *2
		twiceWidth := width *2
		twiceHeight := height *2
		lengthWidthPerimeter := twiceLength + twiceWidth 
		lengthHeightPerimeter := twiceLength + twiceHeight
		widthHeightPerimeter := twiceWidth + twiceHeight

		minimumFace := 0
		if lengthWidthPerimeter <= lengthHeightPerimeter {
			if lengthWidthPerimeter <= widthHeightPerimeter {
				minimumFace = lengthWidthPerimeter
			} else {
				minimumFace = widthHeightPerimeter
			}
		} else if lengthHeightPerimeter <= widthHeightPerimeter {
			minimumFace = lengthHeightPerimeter
		} else {
			minimumFace = widthHeightPerimeter
		}

		ribbonForThisPresent := minimumFace + volume
		lengthOfRibbonAsFeet += ribbonForThisPresent
	}

	fmt.Println( lengthOfRibbonAsFeet )
}

















