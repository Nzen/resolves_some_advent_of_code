package main
 
import (
	"bufio"
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)
 
func main() {
	if len( os.Args ) < 2 {
		log.Fatal( "Error: need filepath argument" )
	}
	filePath := os.Args[ 1 ]
	fileLines := readLines( filePath )

	deriveHashInput( fileLines )
}

func readLines( filepath string ) []string {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	var lines []string

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if scanner.Err() != nil {
		log.Fatal(scanner.Err())
	}
	return lines
}

func deriveHashInput( fileLines []string ) { 
	desiredPrefix := "00000"
	for _, line := range fileLines {
		if len( line ) < 1 {
			continue
		}
		input := ""
		for suffix := 1; suffix < 1_000_000_000; suffix += 1 {
			input = line + strconv.Itoa( suffix )
			byteInput := []byte( input )
			byteOutput := md5.Sum( byteInput )
			result := hex.EncodeToString( byteOutput[:] )
			// fmt.Println( "\t"+ result )
			if strings.HasPrefix( result, desiredPrefix ) {
				fmt.Println( "\t"+ result )
				break
			} 
		}
		fmt.Println( input )
	}
	
	//fmt.Println( "\tresult" )
}

















