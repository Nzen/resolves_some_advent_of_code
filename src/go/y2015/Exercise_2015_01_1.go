package main
 
import (
	"bufio"
	"fmt"
	"log"
	"os"
)
 
func main() {
	if len( os.Args ) < 2 {
		log.Fatal( "Error: need filepath argument" )
	}
	filePath := os.Args[ 1 ]
	fileLines := readLines( filePath )

	countParenthesis( fileLines )
}

func readLines( filepath string ) []string {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	var lines []string

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if scanner.Err() != nil {
		log.Fatal(scanner.Err())
	}
	return lines
}

func countParenthesis( fileLines []string ) {
	for _, line := range fileLines {
		if len( line ) < 1 {
			continue
		}
		// fmt.Println( line )
		var floor = 0
		for _, character := range line {
			// fmt.Println( character )
			if character == '(' {
				floor += 1
			} else if character == ')' {
				floor -= 1
			}
		}
		fmt.Println( floor )
	}
	
	fmt.Println( "\tresult" )
}

















