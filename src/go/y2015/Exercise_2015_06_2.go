package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type Activity int
type Light int

const (
	DISABLE           Activity = iota
	ENABLE            Activity = iota
	TOGGLE            Activity = iota
	UNKNOWN           Activity = iota
	wordForDeliberate          = "turn"
	wordForDisable             = "off"
	wordForEnable              = "on"
	wordForToggle              = "toggle"
	ON                         = Light(1)
	OFF                        = Light(-1)
	DARK                        = Light(0)
	 minimumCoordinate          = 0
	 maximumCoordinate          = 9 // 99
)

type Point struct {
	xx, yy int
}

type Command struct {
	effect              Activity
	topLeftPosition     Point
	bottomRightPosition Point
}

func (light Light) brightness() int {
	return int(light)
}

func (light *Light) toggle() {
	*light += ON + ON
}

func (light *Light) turnOn() {
	*light += ON
}

func (light *Light) turnOff() {
	*light += OFF
}

func parseAsPoint(textCoordinates string) Point {
	xxAndYy := strings.Split(textCoordinates, ",")
	xx, xxWorked := strconv.Atoi(xxAndYy[0])
	yy, yyWorked := strconv.Atoi(xxAndYy[1])
	if xxWorked != nil || yyWorked != nil {
		return Point{-1, -1}
	} else {
		return Point{xx, yy}
	}
}

func parseAsCommand(line string) Command {
	wordsOfLine := strings.Split(line, " ")
	wolIndex := 0

	var doThis Activity
	if wordsOfLine[wolIndex] == wordForToggle {
		doThis = TOGGLE
	} else if wordsOfLine[wolIndex] == wordForDeliberate {
		wolIndex += 1
		if wordsOfLine[wolIndex] == wordForEnable {
			doThis = ENABLE
		} else if wordsOfLine[wolIndex] == wordForDisable {
			doThis = DISABLE
		} else {
			invalidPoint := Point{-1, -1}
			return Command{UNKNOWN, invalidPoint, invalidPoint}
		}
	} else {
		invalidPoint := Point{-1, -1}
		return Command{UNKNOWN, invalidPoint, invalidPoint}
	}

	firstCoordinate := parseAsPoint(wordsOfLine[wolIndex+1])
	secondCoordinate := parseAsPoint(wordsOfLine[wolIndex+3])
	if firstCoordinate.xx > secondCoordinate.xx ||
		firstCoordinate.yy > secondCoordinate.yy {
		firstCoordinate, secondCoordinate = secondCoordinate, firstCoordinate
	}

	return Command{doThis, firstCoordinate, secondCoordinate}
}

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Error: need filepath argument")
	}
	filePath := os.Args[1]
	fileLines := readLines(filePath)

	lightshow(fileLines)
}

func readLines(filepath string) []string {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	var lines []string

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if scanner.Err() != nil {
		log.Fatal(scanner.Err())
	}
	return lines
}

func lightshow(fileLines []string) {
	testing := false
	grid := make(map[Point]Light)
	lit := 0

	for _, line := range fileLines {
		if len(line) < 1 {
			continue
		}
		// fmt.Println("testing " + line)

		directive := parseAsCommand(line)
		if directive.effect == UNKNOWN { // IMPROVE or positions are greater than maximum
			fmt.Println("invalid command : " + line)
			continue
		}

		for lightXx := directive.topLeftPosition.xx; lightXx <= directive.bottomRightPosition.xx; lightXx += 1 {
			for lightYy := directive.topLeftPosition.yy; lightYy <= directive.bottomRightPosition.yy; lightYy += 1 {
				coordinate := Point{lightXx, lightYy}
				lightStatus, found := grid[coordinate]
				if found {
					if directive.effect == TOGGLE {
						lightStatus.toggle()
					} else if directive.effect == ENABLE {
						lightStatus.turnOn()
					} else if directive.effect == DISABLE && lightStatus > DARK {
						lightStatus.turnOff()
					}
					grid[coordinate] = lightStatus
				} else {
					if directive.effect == ENABLE {
						grid[coordinate] = Light(ON)
					} else if directive.effect == TOGGLE {
						grid[coordinate] = Light(ON + ON)
					} else {
						grid[coordinate] = Light(DARK)
					}
				}
			}
		}
		if testing {
				 showGrid(grid)
				lit = 0
				for _, light := range grid {
					lit += light.brightness()
				}
				fmt.Println(lit)
		}
	}

	lit = 0
	for _, light := range grid {
		lit += light.brightness()
	}
	fmt.Println(lit)
}

// *
func showGrid(grid map[Point]Light) {
	var shown strings.Builder

	shown.WriteString( "\n" )
	for lightXx := minimumCoordinate; lightXx <= maximumCoordinate; lightXx += 1 {
		shown.WriteString( strconv.Itoa(lightXx) )
	}
	shown.WriteString( "\n---\n" )

	for lightYy := minimumCoordinate; lightYy <= maximumCoordinate; lightYy += 1 {
		for lightXx := minimumCoordinate; lightXx <= maximumCoordinate; lightXx += 1 {
			coordinate := Point{lightXx, lightYy}
			light, exists := grid[coordinate]
			if !exists || light == DARK {
				shown.WriteRune(' ')
			} else if light < DARK {
				shown.WriteRune('*')
			} else {
				shown.WriteRune( rune( light + Light(48) ) )
			}
		}
		shown.WriteRune('\n')
	}
	shown.WriteString( "\n---\n" )

	fmt.Println(shown.String())
}
// */
