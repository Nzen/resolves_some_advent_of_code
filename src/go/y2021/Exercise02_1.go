package main
 
import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)
 
func main() {
	if len( os.Args ) < 2 {
		log.Fatal( "Error: need filepath argument" )
	}
	filePath := os.Args[ 1 ]
	fileLines := readLines( filePath )

	calculateDepth( fileLines )
}

func readLines( filepath string ) []string {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	var lines []string

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if scanner.Err() != nil {
		log.Fatal(scanner.Err())
	}
	return lines
}

func calculateDepth( fileLines []string ) {
	horizontalPosition := 0
	depth := 0
	dirInd := 0
	valInd := dirInd +1
	for _, line := range fileLines {
		if len( line ) < 1 {
			continue
		}
		
		direction_magnitude := strings.Split( line, " " )
		magnitude, err := strconv.Atoi( direction_magnitude[ valInd ] )
		if err != nil {
			log.Fatal( "not an int ", direction_magnitude[ valInd ] ) // very paranoid, le sigh
		}
		switch direction_magnitude[ dirInd ] {
		case "up" :
			depth -= magnitude
		case "down" :
			depth += magnitude
		// fallthrough if I want that behavior
		default : // forward
			horizontalPosition += magnitude
		}
	}
	
	fmt.Println( "\tresult ", ( horizontalPosition * depth ) )
}

















