/* The authors of Solves-Some-Advent-of-Code release this file under cryptographic autonomy v1 license terms. */

package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Error: need filepath argument")
	}
	filePath := os.Args[1]
	fileLines := readLines(filePath)

	xxx(fileLines)
}

func readLines(filepath string) []string {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	var lines []string

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if scanner.Err() != nil {
		log.Fatal(scanner.Err())
	}
	return lines
}

func xxx(fileLines []string) {
	for _, line := range fileLines {
		if len(line) < 1 {
			continue
		}
		// fmt.Printf("line %s\n", line)
	}

	fmt.Printf("\tresult")
}
