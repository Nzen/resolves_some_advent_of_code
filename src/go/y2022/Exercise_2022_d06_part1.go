/* The authors of Solves-Some-Advent-of-Code release this file under cryptographic autonomy v1 license terms. */

package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Error: need filepath argument")
	}
	filePath := os.Args[1]
	fileLines := readLines(filePath)

	findBeginning(fileLines)
}

func findBeginning(fileLines []string) {
	for _, line := range fileLines {
		if len(line) < 1 {
			continue
		}
		// fmt.Printf("line %s\n", line)
		buffer := []rune{'-', '-', '-', '-'}
		for letterIndex, letter := range line {
			buffer[0], buffer[1], buffer[2], buffer[3] = buffer[1], buffer[2], buffer[3], letter
			if letterIndex < 3 {
				continue
			}
			if buffer[0] != buffer[1] &&
				buffer[0] != buffer[2] &&
				buffer[0] != buffer[3] &&
				buffer[1] != buffer[2] &&
				buffer[1] != buffer[3] &&
				buffer[2] != buffer[3] {
				fmt.Printf("\tresult is %d", letterIndex+1)
				break
			}
		}
	}

	fmt.Printf("\tresult")
}

func readLines(filepath string) []string {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	var lines []string

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if scanner.Err() != nil {
		log.Fatal(scanner.Err())
	}
	return lines
}
