
/* The authors of Solves-Some-Advent-of-Code release this file under cryptographic autonomy v1 license terms. */

package main
 
import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
)
 
func main() {
	if len( os.Args ) < 2 {
		log.Fatal( "Error: need filepath argument" )
	}
	filePath := os.Args[ 1 ]
	fileLines := readLines( filePath )

	maxCalories( fileLines )
}

func readLines( filepath string ) []string {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	var lines []string

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if scanner.Err() != nil {
		log.Fatal(scanner.Err())
	}
	return lines
}

func organizeTopThree( topThree []int, currentGroup int ) {
	if currentGroup > topThree[ 0 ] {
		topThree[ 0 ], topThree[ 1 ], topThree[ 2 ] = currentGroup, topThree[ 0 ], topThree[ 1 ]
	} else if currentGroup > topThree[ 1 ] {
		topThree[ 1 ], topThree[ 2 ] = currentGroup, topThree[ 1 ]
	} else if currentGroup > topThree[ 2 ] {
		topThree[ 2 ] = currentGroup
	}
}

func maxCalories( fileLines []string ) {
	topThree := []int{ -1, -1, -1 }
	groupTotal := 0

	for _, line := range fileLines {
		if len( line ) < 1 {
			organizeTopThree( topThree, groupTotal )
			groupTotal = 0
		} else {
			amount, problem := strconv.Atoi( line )
			if problem == nil {
				groupTotal += amount
			}
		}
	}
	if groupTotal > 0 {
		organizeTopThree( topThree, groupTotal )
		groupTotal = 0
	}

	for _, subtotal := range topThree {
		groupTotal += subtotal
	}

	fmt.Println( groupTotal )
}

















