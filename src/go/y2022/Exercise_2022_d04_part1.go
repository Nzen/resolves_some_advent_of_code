/* The authors of Solves-Some-Advent-of-Code release this file under cryptographic autonomy v1 license terms. */

package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Error: need filepath argument")
	}
	filePath := os.Args[1]
	fileLines := readLines(filePath)

	findDuplicates(fileLines)
}

func findDuplicates(fileLines []string) {
	duplicateCount := 0

	for _, line := range fileLines {
		if len(line) < 1 {
			continue
		}
		// fmt.Printf("line %s\n", line)

		lowValueIndex := 0
		highValueIndex := lowValueIndex + 1
		slotRanges := strings.Split(line, ",")
		firstRange := strings.Split(slotRanges[lowValueIndex], "-")
		secondRange := strings.Split(slotRanges[highValueIndex], "-")
		firstBegins, problem := strconv.Atoi(firstRange[lowValueIndex])
		if problem != nil {
			fmt.Printf("problem with %s because %v\n", line, problem)
			continue
		}
		firstEnds, problem := strconv.Atoi(firstRange[highValueIndex])
		if problem != nil {
			fmt.Printf("problem with %s because %v\n", line, problem)
			continue
		}
		secondBegins, problem := strconv.Atoi(secondRange[lowValueIndex])
		if problem != nil {
			fmt.Printf("problem with %s because %v\n", line, problem)
			continue
		}
		secondEnds, problem := strconv.Atoi(secondRange[highValueIndex])
		if problem != nil {
			fmt.Printf("problem with %s because %v\n", line, problem)
			continue
		}

		if (firstBegins >= secondBegins && firstBegins <= secondEnds && firstEnds <= secondEnds) ||
			(secondBegins >= firstBegins && secondBegins <= firstEnds && secondEnds <= firstEnds) {
			duplicateCount += 1
		}
	}

	fmt.Printf("\tresult %d", duplicateCount)
}

func readLines(filepath string) []string {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	var lines []string

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if scanner.Err() != nil {
		log.Fatal(scanner.Err())
	}
	return lines
}
