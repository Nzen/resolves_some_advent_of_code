/* The authors of Solves-Some-Advent-of-Code release this file under cryptographic autonomy v1 license terms. */

package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/schicho/substring"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Error: need filepath argument")
	}
	filePath := os.Args[1]
	fileLines := readLines(filePath)

	scoreCommonItem(fileLines)
}

func readLines(filepath string) []string {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	var lines []string

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if scanner.Err() != nil {
		log.Fatal(scanner.Err())
	}
	return lines
}

func scoreCommonItem(fileLines []string) {
	sumOfDuplicates := 0

	for _, line := range fileLines {
		if len(line) < 1 {
			continue
		}
		// fmt.Println(line)

		// fmt.Printf("line > %s len %d half %d plus %d\n", line, len(line), len(line)/2, (len(line)/2)-1)
		leftPocketContents := substring.Substring(line, 0, len(line)/2)
		rightPocketContents := substring.Substring(line, (len(line) / 2), len(line))
		// fmt.Printf("left %s right %s\n", leftPocketContents, rightPocketContents)

		for _, char := range leftPocketContents {
			rightIndex := strings.IndexRune(rightPocketContents, char)
			if rightIndex >= 0 {
				// fmt.Printf("char %s int %d\n", string(char), valueOf(char))
				sumOfDuplicates += valueOf(char)
				break
			}
		}
	}

	fmt.Println("\tresult ", sumOfDuplicates)
}

func valueOf(char rune) int {
	if char >= 'a' && char <= 'z' {
		return int(char) - 96
	} else if char >= 'A' && char <= 'Z' {
		return int(char) - 38
	} else {
		return -1
	}
}
