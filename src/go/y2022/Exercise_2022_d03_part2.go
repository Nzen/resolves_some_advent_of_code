/* The authors of Solves-Some-Advent-of-Code release this file under cryptographic autonomy v1 license terms. */

package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Error: need filepath argument")
	}
	filePath := os.Args[1]
	fileLines := readLines(filePath)

	scoreCommonItem(fileLines)
}

func readLines(filepath string) []string {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	var lines []string

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if scanner.Err() != nil {
		log.Fatal(scanner.Err())
	}
	return lines
}

func scoreCommonItem(fileLines []string) {
	sumOfDuplicates := 0
	membersOfGroup := 0
	groupLimit := 3
	memberBags := []string{"", "", ""}

	for _, line := range fileLines {
		if len(line) < 1 {
			continue
		}

		memberBags[membersOfGroup] = line
		membersOfGroup += 1

		if membersOfGroup >= groupLimit {
			membersOfGroup = 0
			firstBag := make(map[rune]int)
			secondBag := make(map[rune]int)
			thirdBag := make(map[rune]int)

			for index, char := range memberBags[0] {
				firstBag[char] = index
			}
			for index, char := range memberBags[1] {
				secondBag[char] = index
			}
			for index, char := range memberBags[2] {
				thirdBag[char] = index
			}

			for firstChar, _ := range firstBag {
				_, foundInSecond := secondBag[firstChar]
				if foundInSecond {
					_, foundInThird := thirdBag[firstChar]
					if foundInThird {
						sumOfDuplicates += valueOf(firstChar)
						break
					}
				}
			}
		}
	}

	fmt.Println("\tresult ", sumOfDuplicates)
}

func valueOf(char rune) int {
	if char >= 'a' && char <= 'z' {
		return int(char) - 96
	} else if char >= 'A' && char <= 'Z' {
		return int(char) - 38
	} else {
		return -1
	}
}
