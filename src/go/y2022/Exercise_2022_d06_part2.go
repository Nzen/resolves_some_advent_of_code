/* The authors of Solves-Some-Advent-of-Code release this file under cryptographic autonomy v1 license terms. */

package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"sort"
)

type RuneArray []rune

func (buffer RuneArray) Len() int {
	return len(buffer)
}

func (x RuneArray) Less(i, j int) bool { return x[i] < x[j] }

func (x RuneArray) Swap(i, j int) { x[i], x[j] = x[j], x[i] }

func (buffer RuneArray) hasDuplicate() bool {
	for index, letter := range buffer {
		if index < 1 {
			continue
		} else if letter == buffer[index-1] {
			return true
		}
	}
	return false
}

func main() {
	if len(os.Args) < 2 {
		log.Fatal("Error: need filepath argument")
	}
	filePath := os.Args[1]
	fileLines := readLines(filePath)

	findBeginning(fileLines)
}

func findBeginning(fileLines []string) {
	for _, line := range fileLines {
		if len(line) < 1 {
			continue
		}
		// fmt.Printf("line %s\n", line)
		buffer := RuneArray{'-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-', '-'}
		for letterIndex, letter := range line {
			buffer[0], buffer[1], buffer[2], buffer[3], buffer[4],
				buffer[5], buffer[6], buffer[7], buffer[8], buffer[9],
				buffer[10], buffer[11], buffer[12], buffer[13] =
				buffer[1], buffer[2], buffer[3], buffer[4], buffer[5],
				buffer[6], buffer[7], buffer[8], buffer[9], buffer[10],
				buffer[11], buffer[12], buffer[13], letter
			if letterIndex < 13 {
				continue
			}

			trashable := RuneArray{buffer[0], buffer[1], buffer[2], buffer[3], buffer[4],
				buffer[5], buffer[6], buffer[7], buffer[8], buffer[9],
				buffer[10], buffer[11], buffer[12], buffer[13]}
			sort.Sort(trashable)

			if !trashable.hasDuplicate() {
				fmt.Printf("\tresult is %d", letterIndex+1)
				break
			}
		}
	}
}

func readLines(filepath string) []string {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	var lines []string

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if scanner.Err() != nil {
		log.Fatal(scanner.Err())
	}
	return lines
}
