
/* The authors of Solves-Some-Advent-of-Code release this file under cryptographic autonomy v1 license terms. */

package main


import (
	"bufio"
	"fmt"
	"log"
	"os"
	"strings"
)


type Gesture struct {
	value int
}


type Outcome struct {
	value int
}


var paper Gesture
var rock Gesture
var scissors Gesture
var gun Gesture
var win Outcome
var lose Outcome
var draw Outcome
var confusion Outcome


func init() {
	paper = Gesture{ 2 }
	rock = Gesture{ 1 }
	scissors = Gesture { 3 }
	gun = Gesture { -1 }
	win = Outcome{ 6 }
	lose = Outcome{ 0 }
	draw = Outcome{ 3 }
	confusion = Outcome{ -1 }
}


func isMyPlay( symbol string ) bool {
	return symbol == "X" ||
		symbol == "Y" ||
		symbol == "Z"
}


func gameOutcome( myPlay Gesture, theirPlay Gesture ) Outcome {
	if myPlay == theirPlay {
		return draw
	} else if myPlay == rock && theirPlay == paper ||
			myPlay == paper && theirPlay == scissors ||
			myPlay == scissors && theirPlay == rock {
		return lose
	} else if myPlay == rock && theirPlay == scissors ||
			myPlay == paper && theirPlay == rock ||
			myPlay == scissors && theirPlay == paper {
		return win
	} else {
		return confusion
	}
}


func gestureGame( fileLines []string ) {
	finalScore := 0
	for _, line := range fileLines {
		if len( line ) < 1 {
			continue
		}
		// fmt.Println( line )
		cheat := strings.Split( line, " " )

		theirs := gestureOf( cheat[ 0 ] )
		outcome := outcomeOf( cheat[ 1 ] )
		mine := gestureTo( outcome, theirs )

		finalScore += outcome.value + mine.value
	}
	
	fmt.Printf( "\tresult %d", finalScore )
}


func gestureOf( symbol string ) Gesture {
	switch symbol {
	case "X", "A" :
		return rock
	case "Y", "B" :
		return paper
	case "Z", "C" :
		return scissors
	default :
		return gun
	}
}


func gestureTo( outcome Outcome, theirPlay Gesture ) Gesture {
	switch outcome {
	case win :
		if theirPlay == paper {
			return scissors
		} else if theirPlay == rock {
			return paper
		} else if theirPlay == scissors {
			return rock
		} else {
			return gun
		}
	case lose :
		if theirPlay == paper {
			return rock
		} else if theirPlay == rock {
			return scissors
		} else if theirPlay == scissors {
			return paper
		} else {
			return gun
		}
	case draw :
		return theirPlay
	default :
		return gun
	}
}


func main() {
	if len( os.Args ) < 2 {
		log.Fatal( "Error: need filepath argument" )
	}
	filePath := os.Args[ 1 ]
	fileLines := readLines( filePath )

	gestureGame( fileLines )
}


func outcomeOf( symbol string ) Outcome {
	switch symbol {
	case "X" :
		return lose
	case "Y" :
		return draw
	case "Z" :
		return win
	default :
		return confusion
	}
}


func readLines( filepath string ) []string {
	file, err := os.Open(filepath)
	if err != nil {
		log.Fatal(err)
	}

	defer file.Close()

	var lines []string

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if scanner.Err() != nil {
		log.Fatal(scanner.Err())
	}
	return lines
}

















