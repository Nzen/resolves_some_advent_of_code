
/* The authors of Solves-Some-Advent-of-Code release this file under cryptographic autonomy v1 license terms. */

/* fileLines is an array */
function countFloors( fileLines ) {
	var currentFloor = 0;

	var asSingleMovements = fileLines[0].split( "" );
	for ( var index = 0; index < asSingleMovements.length; index += 1 ) {
		var movement = asSingleMovements[ index ];
		if ( movement == "(" )
			currentFloor += 1;
		else if ( movement == ")" )
			currentFloor -= 1;
		else
			console.log( "Invalid floor movement character, "+ movement +"\n" );
	}

	return currentFloor;
}

function handleIngestedFile() {
	var entireFile = this.response;
	var asLines = entireFile.split( "\n" );
	var answer = countFloors( asLines );
	document.getElementById('answer').innerHTML = "The answer is " + answer;
}

function readTextfile( nameOfFile ) {
	var ajax = new XMLHttpRequest;
	ajax.open( 'GET', nameOfFile );
	ajax.onload = handleIngestedFile;
	ajax.send();
}

function doMainThing() {
	readTextfile( 'http://localhost:11131/res/y2015/2015_01_input.txt' );
}



















