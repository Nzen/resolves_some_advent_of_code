
== Answers folder

The input is not worth much without correct answers to calibrate them.
Given that this is well away from the inputs and sources, its temptation as information hazard is yours to manage.

Each answer file has the same name and path as the resource folder it corresponds to.
